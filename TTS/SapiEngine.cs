﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
using System.Collections.ObjectModel;

#if WINDOWS
namespace SignalCenter.TTS
{

    //  Handles Speech Synth
    public class SapiEngine : ISpeechEngine
    {
        public SapiEngine()
        {
            _Synth = new System.Speech.Synthesis.SpeechSynthesizer();
            _Synth.SetOutputToDefaultAudioDevice();
            _Synth.SelectVoiceByHints(System.Speech.Synthesis.VoiceGender.Male, System.Speech.Synthesis.VoiceAge.Adult);
            _Prompt = null;
        }

        public List<String> GetMaleVoices(){
            IEnumerable<System.Speech.Synthesis.InstalledVoice> installedMaleVoices =   _Synth.GetInstalledVoices().Where(a => a.VoiceInfo.Gender == System.Speech.Synthesis.VoiceGender.Male);
             
            return installedMaleVoices.Select<System.Speech.Synthesis.InstalledVoice, String>(a => a.VoiceInfo.Name.ToString()).ToList<String>();
        }

        public List<String> GetFemaleVoices()
        {
            IEnumerable<System.Speech.Synthesis.InstalledVoice> installedFemaleVoices =   _Synth.GetInstalledVoices().Where(a => a.VoiceInfo.Gender == System.Speech.Synthesis.VoiceGender.Female);
             
            return installedFemaleVoices.Select<System.Speech.Synthesis.InstalledVoice, String>(a => a.VoiceInfo.Name.ToString()).ToList<String>();
        }
   

        public List<String> GetVoices()
        {
            List<string> voices = new List<string>();
                voices.AddRange(GetMaleVoices());
                voices.AddRange(GetFemaleVoices());

                IEnumerable<System.Speech.Synthesis.InstalledVoice> installedOtherVoices = _Synth.GetInstalledVoices().Where(a => a.VoiceInfo.Gender != System.Speech.Synthesis.VoiceGender.Female && a.VoiceInfo.Gender != System.Speech.Synthesis.VoiceGender.Male);

                List<String> otherVoices = installedOtherVoices.Select<System.Speech.Synthesis.InstalledVoice, String>(a => a.VoiceInfo.Name.ToString()).ToList<String>();

                voices.AddRange(otherVoices);

                return voices;
        }

        /// <summary>
        /// Plays speech. The playing should not stop until all previous have stopped or been cancelled.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="mode"></param>
        /// <param name="voice"></param>
        /// <returns></returns>
        public void Play(String text, Configuration.Policies mode, Configuration.Voices voice)
        {
            Play(text, mode, voice, SignalCenter.Common.UserManager.Instance.CurrentUser.PrefTTSRate);
        }

        /// <summary>
        /// Plays speech. The playing should not stop until all previous have stopped or been cancelled.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="mode"></param>
        /// <param name="voice"></param>
        /// <returns></returns>
        public void Play(String text, Configuration.Policies mode)
        {
            Play(text, mode, Configuration.Voices.Female, SignalCenter.Common.UserManager.Instance.CurrentUser.PrefTTSRate);
        }
        public void Play(String text, Configuration.Policies mode, Configuration.Voices voice, int rate)
        {
            if (mode == Configuration.Policies.Sentences || mode == Configuration.Policies.Words)
            {
                _Synth.Rate = rate;
                SetVoice(voice);
                System.Speech.Synthesis.PromptBuilder p = new System.Speech.Synthesis.PromptBuilder();
                _Prompt = _Synth.SpeakAsync(text);
            }
        }

        /// <summary>
        /// Cancels all speech.
        /// </summary>
        public void Cancel()
        {
            _Synth.SpeakAsyncCancel(_Prompt);
            _Prompt = null;
        }

        /// <summary>
        /// Checks if speech is playing.
        /// </summary>
        public bool IsPlaying()
        {
            return !(_Prompt == null || _Prompt.IsCompleted);
        }

        /// <summary>
        /// Sets the voice to use.
        /// </summary>
        /// <param name="voice"></param>
        private void SetVoice(Configuration.Voices voice)
        {
            String voiceName = String.Empty;
            if (voice == Configuration.Voices.Female)
            {
                voiceName = SignalCenter.Common.UserManager.Instance.CurrentUser.PrefFemaleVoice;
            }
            else if (voice == Configuration.Voices.Male)
            {
                voiceName = SignalCenter.Common.UserManager.Instance.CurrentUser.PrefMaleVoice;
            }

            _Synth.SelectVoice(voiceName);

        }
        private System.Speech.Synthesis.SpeechSynthesizer _Synth;

        private System.Speech.Synthesis.Prompt _Prompt;

    }

}
#endif
