﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SignalCenter.Common;
using SignalCenter.TTS.Sound;
using SignalCenter.TTS;
using SignalCenter.TTS.Configuration;

namespace SignalCenter.TTS
{
    public class GeneralHelp
    {
        private int TimeoutInMilliseconds = 700;

        public GeneralHelp()
        {
        }

        public void Start()
        {
            using (SoundPlayerNode node = new SoundPlayerNode("HelpStart.wav", true, Voices.Informative, TimeoutInMilliseconds))
            {
                SoundPlayer.Instance.QueueSingleSoundNode(node);
            }

            using (SoundPlayerNode node = new SoundPlayerNode("HelpMainMenu.wav", true, Voices.Informative, TimeoutInMilliseconds))
            {
                SoundPlayer.Instance.QueueSingleSoundNode(node);
            }
        }

        public void KeyPressed(Keys key)
        {
            switch (key)
            {
                case Keys.D0:
                case Keys.NumPad0:
                    PlayHelpFile("HelpMainMenu.wav");
                    break;
                case Keys.D1:
                case Keys.NumPad1:
                    PlayHelpFile("HelpShortcutKeys.wav");
                    break;
                case Keys.D2:
                case Keys.NumPad2:
                    PlayHelpFile("HelpVoices.wav");
                    break;
                case Keys.D3:
                case Keys.NumPad3:
                    PlayHelpFile("HelpLessonOrder.wav");
                    break;
                case Keys.D4:
                case Keys.NumPad4:
                    PlayHelpFile("HelpSelectingLesson.wav");
                    break;
                case Keys.D5:
                case Keys.NumPad5:
                    PlayHelpFile("HelpCharactersLesson.wav");
                    break;
                case Keys.D6:
                case Keys.NumPad6:
                    PlayHelpFile("HelpWordsOrSentencesLesson.wav");
                    break;
                case Keys.D7:
                case Keys.NumPad7:
                    PlayHelpFile("HelpLessonCompleted.wav");
                    break;
            }
        }

        public void Stop()
        {
            SoundPlayer.Instance.CancelAll();
            PlaySoundFile("HelpExited.wav");
        }

        private void PlayHelpFile(string fileName)
        {
            SoundPlayer.Instance.CancelAll();
            System.Threading.Thread.Sleep(TimeoutInMilliseconds);
            PlaySoundFile(fileName);
            PlaySoundFile("HelpReturnOrExit.wav");
        }

        private void PlaySoundFile(string fileName)
        {
            using (SoundPlayerNode node = new SoundPlayerNode(fileName, true, Voices.Informative, TimeoutInMilliseconds))
            {
                SoundPlayer.Instance.QueueSingleSoundNode(node);
            }
        }
    }
}
