
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#define WINDOWS

using System.Text;
using System;
using System.Collections.Generic;
using SignalCenter.Common.Util;
using System.Collections.ObjectModel;
using SignalCenter.Common;


namespace SignalCenter.TTS
{
    
    //  Handles Speech Synth
    public static class Engine
    {
        static Engine()
        {
            // Use builtin speech synthesis if the type System.Speech.Synthesis.SpeechSynthesizer exists.
#if MAC_OSX
                 _Impl = new SayEngine() as ISpeechEngine;
#else
                 _Impl = new SapiEngine() as ISpeechEngine;      
#endif
         }
      
        /// <summary>
        /// Plays speech. The playing should not stop until all previous have stopped or been cancelled.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="mode"></param>
        /// <param name="voice"></param>
        /// <returns></returns>
        public static void Play(String text, Configuration.Policies mode, Configuration.Voices voice)
        {
            lock (_Impl)
            {
                if (_Impl.IsPlaying())
                {
                    _Impl.Cancel();
                }
                _Impl.Play(text, mode, voice);
            }
        }

        public static List<String> GetVoices()
        {
            return _Impl.GetVoices();
        }

        public static List<String> GetFemaleVoices()
        {
            return _Impl.GetFemaleVoices();
        }

        public static List<String> GetMaleVoices()
        {
            return _Impl.GetMaleVoices();
        }
        /// <summary>
        /// Plays speech. The playing should not stop until all previous have stopped or been cancelled.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="mode"></param>
        /// <param name="voice"></param>
        /// <returns></returns>
        public static void Play(String text, Configuration.Policies mode)
        {
            lock (_Impl)
            {
                if (_Impl.IsPlaying())
                {
                    _Impl.Cancel();
                }
                _Impl.Play(text, mode);
            }
        }
        public static void Play(String text, Configuration.Policies mode, Configuration.Voices voice, int rate)
        {
            lock (_Impl)
            {
                if (_Impl.IsPlaying())
                {
                    _Impl.Cancel();
                }
                _Impl.Play(text, mode, voice, rate);
            }
        }

        /// <summary>
        /// Cancels all speech.
        /// </summary>
        public static void Cancel()
        {
            lock (_Impl)
            {
                if (_Impl.IsPlaying())
                {
                    _Impl.Cancel();
                }
            }
        }

        /// <summary>
        /// Checks if speech is playing.
        /// </summary>
        public static bool IsPlaying()
        {
            lock (_Impl)
            {
                return _Impl.IsPlaying();
            }
        }
        private static ISpeechEngine _Impl;
    }
}
