﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Concurrent;
using SignalCenter.Common.Util;
namespace SignalCenter.TTS.Sound
{
    /// <summary>
    /// Provides coupling the AbsoluteFilePath to the Wave with the Voice it is to be played with.
    /// Furthermore, allows for a playback pause to be specified. This playback pause is how long
    /// the thread will wait, in millis, after starting the call to play the wave file.
    /// </summary>
    public class SoundPlayerNode : IDisposable
    {
        public readonly SignalCenter.TTS.Configuration.Voices Voice;
        public readonly String AbsoluteFilePath;
        public readonly int PlaybackTimeout;
        public          EventHandler SoundCompleted;

        public SoundPlayerNode(String fileName, int playback)
            :  this(fileName, false, TTS.Configuration.Voices.Informative, playback, null)
        {
        }

        /// <summary>
        /// Constructs a node for a sound player instance. Sets the SoundCompleted event handler to null. 
        /// </summary>
        /// <param name="fileName">Either a .WAV filename or a complete path with a .WAV included.</param>
        /// <param name="doAdjustPath">If true, adjusts the fileName to a complete path based on the voice parameter.</param>
        /// <param name="voice">The voice to play the .WAV file with.</param>
        /// <param name="playback"> Amount of time, ms, to wait after completion of the WAV sound.</param>
        public SoundPlayerNode(String fileName, Boolean doAdjustPath, SignalCenter.TTS.Configuration.Voices voice, int playback)
            :this(fileName,doAdjustPath,voice,playback,null)
        {
            
        }


        public SoundPlayerNode(String fileName, Boolean doAdjustPath, SignalCenter.TTS.Configuration.Voices voice, int playback, EventHandler soundCompleted)
        {
            Voice = voice;
            string tempFilePath;
            if (doAdjustPath)
                tempFilePath = voice.GetFilePath(String.Empty, fileName);
            else
                tempFilePath = fileName;

            AbsoluteFilePath = tempFilePath;
            PlaybackTimeout = playback;
            SoundCompleted = soundCompleted;
        }

        /// <summary>
        /// Checks if the  AbsoluteFilePath property is valid.
        /// </summary>
        /// <returns></returns>
        public Boolean CheckFile()
        {
            return System.IO.File.Exists(AbsoluteFilePath);
        }
        public void Dispose()
        {
        
        }
    }
}