
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;

namespace SignalCenter.TTS.Sound
{
	public class MacSoundUtility : ISound
	{
		private MonoMac.AppKit.NSSound sound;

		public MacSoundUtility ()
		{
			SoundCompletedSubscribers = new SoundCompletedObserver();
		}

		private SoundCompletedObserver SoundCompletedSubscribers; 


		public Boolean IsPlaying 
		{
			get
			{
				if(sound != null)
				{
					return sound.IsPlaying();
				}
				else
				{
					return false;
				}
			}
		}

		public void AddSubscriber(EventHandler h)
		{
			SoundCompletedSubscribers.AddListener(h);
		}

		public Boolean Stop()
		{

			if(sound != null)
			{
				return sound.Stop();
			}
			else
			{
				return false;
			}
		}

		private void Threaded_Play()
		{
			sound.DidFinishPlaying += new EventHandler<MonoMac.AppKit.NSSoundFinishedEventArgs>(HandleWavFinished);
			sound.Play();

		}
		public void Play()
		{

			if(sound != null)
			{
				new System.Threading.Thread(new System.Threading.ThreadStart(Threaded_Play)).Start();
			}
		}

		private void HandleWavFinished(object sender, MonoMac.AppKit.NSSoundFinishedEventArgs e)
		{
			Console.WriteLine("A");
			OnWavCompleted(e.Finished);
		}

		public Boolean Load(String path)
		{
			Stop ();
			sound = new MonoMac.AppKit.NSSound(path, byRef: false);

			return true;
		}

		public void OnWavCompleted(Boolean canceled)
		{
			Console.WriteLine("WavCompleted");
			SoundCompletedSubscribers.FireEvent(this, new SoundBufferCompletedEventArgs(canceled));
		}
	}
}

