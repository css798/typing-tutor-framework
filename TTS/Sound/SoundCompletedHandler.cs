﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignalCenter.TTS.Sound
{

//    public delegate void SoundBufferCompletedEventHandler(object sender, SoundBufferCompletedEventArgs e);
   

    public class SoundCompletedObserver
    {
     
    private List<EventHandler> soundBufferCompletedListeners;

    public SoundCompletedObserver()
    {
        soundBufferCompletedListeners = new List<EventHandler>();
    }

     public void AddListener(EventHandler listener)
     {
        if (!soundBufferCompletedListeners.Contains(listener) && listener != null)
         {
             soundBufferCompletedListeners.Add(listener);
         }
     }

     public void RemoveListener(EventHandler  listener)
     {

         if(soundBufferCompletedListeners.Contains(listener))
         {
             soundBufferCompletedListeners.Remove(listener);
         }
     }


     public void FireEvent(object sender, EventArgs e)
     {
         foreach(EventHandler h in soundBufferCompletedListeners)
         {
             h.BeginInvoke(sender, e, null,null);
         }

         soundBufferCompletedListeners.Clear();

     }

    }

    public class SoundBufferCompletedEventArgs : EventArgs
    {
        /// <summary>
        /// Represents if the sound buffer was cut short pre maturely.
        /// </summary>
        public Boolean SoundCanceled { get; set; }

        public SoundBufferCompletedEventArgs(Boolean bufferSkipToEnd)
        {
            SoundCanceled = bufferSkipToEnd;
        }
    }
}
