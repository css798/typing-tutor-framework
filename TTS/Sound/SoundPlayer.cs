﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Concurrent;
using SignalCenter.TTS.Sound.WaveLib;
using SignalCenter.Common.Util;

namespace SignalCenter.TTS.Sound
{
    /// <summary>
    /// Provides a Wrapper for Playing Wav files.
    /// Every instantion is placed on a background thread. 
    /// Furthermore, canceling a thread is a simple as <code>mySpeechPlayer.isTerminated = false;</code>
    /// </summary>
    public class SoundPlayer : IDisposable
    {

        #region Members
        /// <summary>
        /// Gets the singleton instance of SoundPlayer.
        /// </summary>
        public static SoundPlayer Instance { get { return _instance; } }
        private static SoundPlayer _instance = new SoundPlayer();

		#if MAC_OSX
			private ISound sound = new MacSoundUtility();
		#elif WINDOWS
			private ISound sound = new SoundUtility();
		#endif
        /// <summary>
        /// Contains a collection of the current playing sounds.
        /// </summary>
        //private static readonly HashSet<SoundUtility> CurrentPlayingSounds;

        /// <summary>
        /// Allows canceling the worker thread. Worker thread will stay alive as long as this is set to false.
        /// </summary>
        private Boolean isTerminated;

        /// <summary>
        /// Signifies the worker thread is currently paused and awaiting either a sound player node or is awaiting a sound to finish playing.
        /// </summary>
        private Nullable<Boolean> isPaused;

        /// <summary>
        /// A queue that holds all the WAV sound information to be played.
        /// </summary>
        internal readonly ConcurrentQueue<SoundPlayerNode> PlayerQueue;


        public Boolean IsPlaying { get 
        {
            //Console.WriteLine("Sound is " + sound.IsPlaying);
            return sound.IsPlaying; }
        }

        private System.Threading.Thread workerThread;
        #endregion

        #region Constructors
     
        /// <summary>
        /// Constructs a new sound player. A sound player has a worker thread that dequeues from it's queue and plays a WAV sound.
        /// </summary>
        private SoundPlayer()
        {


            isPaused = null;
            PlayerQueue = new ConcurrentQueue<SoundPlayerNode>();
           
            workerThread = new System.Threading.Thread(new System.Threading.ThreadStart(DoWork));
			workerThread.Name = "Sound Thread";
            workerThread.Start();
        }

        
        #endregion

        /// <summary>
        /// Plays a single wav file. 
        /// Constructs a new SoundUtility instance.
        /// Adjusts the file path if specified by the adjustFilePath parameter within the node.
        /// Registers for the End of Buffer Completed Event and Plays the WAV.
        /// 
        /// </summary>
        /// <param name="node">an instance of soundplayernode that specifies properties to play the WAV</param>
        /// <param name="soundCompleted">EventHadler instance that specifies handling the end of the WAV buffer. Nulls are permitted.</param>
		private Boolean  PlaySingleWav(SoundPlayerNode node, EventHandler soundCompleted)
		{
            Boolean results = true;
            lock (sound)
            {
                if (sound.IsPlaying)
                {
                    sound.Stop();
                }
                try
                {
                    sound.AddSubscriber(soundCompleted);
                    sound.AddSubscriber(node.SoundCompleted);

                    sound.Load(node.AbsoluteFilePath);
                    sound.Play();
                    //Console.WriteLine("Sound is {0}. Should be true", sound.IsPlaying);

                    results = true;

                }
                catch (Exception e)
                {
                    results = false;
                }
            }
                 return results;
        }
        
        #region Public Methods
      
        
        /// <summary>
        /// Adds a SoundPlayerNode to the Player. This method is thread safe. Wakes up worker thread to play sound.
        /// </summary>
        /// <param name="speechItem"></param>
        /// <returns>If true, the file exists and was successfully queued.</returns>
        public Boolean QueueSingleSoundNode(SoundPlayerNode speechItem)
        {
            Boolean results = false;
            if (speechItem.CheckFile())
            {
                PlayerQueue.Enqueue(speechItem);
                isPaused = false;
                //Wake up WorkerThread.
                lock (PlayerQueue)
                {
                    System.Threading.Monitor.PulseAll(PlayerQueue);
                }

                results = true;
            }


            return results;
        }

        /// <summary>
        /// Cancels only the current sound.
        /// </summary>
        public void Cancel()
        {
            sound.Stop();

            Console.WriteLine("Sound: {0}=false", sound.IsPlaying);
        }

        /// <summary>
        /// Cancels current sound along with any sounds currently queued.
        /// </summary>
        public void CancelAll()
        {
           //Synchronize the queue so that when the sound is canceled another sound is not played before it can be dequeued.
            lock (PlayerQueue)
            {
                Cancel();

                SoundPlayerNode n;
                isPaused = true;

                while (!this.PlayerQueue.IsEmpty)
                {
                    if (this.PlayerQueue.TryDequeue(out n))
                    {
                        Console.WriteLine("DEBUG: Removing - " + System.IO.Path.GetFileName(n.AbsoluteFilePath));
                    }
                }
            }
           
        }
        /// <summary>
        /// Disposes of resources.
        /// </summary>
        public virtual void Dispose()
        {
            Cancel();
            isTerminated = true;
            isPaused = false;
            lock (PlayerQueue)
            {
                System.Threading.Monitor.PulseAll(PlayerQueue);
            }
            workerThread.Join();
            

        }


        #endregion

        #region Private Methods
     
        /// <summary>
        /// Handles Playing all Files located in the LinkedList. 
        /// Additionally, plays asynchronously.
        /// </summary>
        protected void DoWork()
        {
            //System.Threading.Thread.CurrentThread.Name = "Speech Player Thread";
            Console.WriteLine("Thread Started");
            while (!isTerminated)
            {
                   CheckAndWait();

                SoundPlayerNode speechPlayerNode = null;
                if (PlayerQueue.Count > 0)
                {
                    //Dequeue the first element in the queue.  Then the thread will wait until either
                    //      1. The sound has finished playing.
                    //      2. A request to remove all elements from the queue.
                    if (PlayerQueue.TryDequeue(out speechPlayerNode))
                    {
                        Boolean playingSuccess = PlaySingleWav(speechPlayerNode, new EventHandler(sound_SoundBufferCompleted));
                        if (playingSuccess)
                        {
                            isPaused = true;
                           
							CheckAndWait();
                           
							System.Threading.Thread.Sleep(speechPlayerNode.PlaybackTimeout);
                        }

                    }
                }else
				{
					isPaused = true;
				}
            } 
        }
       

        /// <summary>
        /// Handles waking the sound player and removing the instance of sound utility that has finished.
        /// </summary>
        /// <param name="sender">Sound Utility instance</param>
        /// <param name="e">The event args.</param>
        private void sound_SoundBufferCompleted(object sender, EventArgs e)
        {
            isPaused = false;
            lock (PlayerQueue)
            {
                System.Threading.Monitor.PulseAll(PlayerQueue);
            }
        }
  
        /// <summary>
        /// Provides a waiting until a Monitor.Pulse(player) or Monitor.PulsAll(player) is called.
        /// </summary>
        private void CheckAndWait()
        {
            lock (PlayerQueue)
            {
                while (isPaused == true)
                {
                    System.Threading.Monitor.Wait(PlayerQueue);
                }
            }
        }
       
        #endregion

    }
}