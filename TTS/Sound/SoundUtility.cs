﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using SignalCenter.TTS.Sound.WaveLib;


namespace SignalCenter.TTS.Sound
{
    public class SoundUtility : ISound
    {
        private WaveOutPlayer m_Player;
        private WaveFormat    m_Format;
        private System.IO.Stream      m_AudioStream;
        private WaveLib.BufferFillEventHandler m_Buffer_Fill;

        /// <summary>
        /// This observer pattern implementation is a fire and forget subscribers. The provider will only remember its subscribers for one event instance.
        /// </summary>
		private Sound.SoundCompletedObserver SoundCompletedObserver;

        public Boolean IsPlaying
        {
            get
            {
                if (m_Player == null)
                {
                    return false;
                }
                else
                {
                    return m_Player.IsPlaying;
                }

            }
        }


        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
       internal SoundUtility()
       {
            SoundCompletedObserver = new Sound.SoundCompletedObserver();
           
			m_Buffer_Fill = new BufferFillEventHandler(this.Filler);
       }      
        #endregion

		public void AddSubscriber(EventHandler h)
		{
			SoundCompletedObserver.AddListener(h);
		}

        private void Filler(IntPtr data, int size, ref Boolean isFinished, ref Boolean isBufferEnd)
        {
            byte[] b = new byte[size];
            if (m_AudioStream != null)
            {
                int pos = 0;
                while (pos < size)
                {
                    int toget = size - pos;
                    int got = m_AudioStream.Read(b, pos, toget);
                  //  if (got < toget)
                    if(m_AudioStream.Position == m_AudioStream.Length || isFinished)
                    {
                        isFinished = true; // notify end of playing
                        isBufferEnd = true;
                        m_AudioStream.Position = 0; // loop if the file ends
                        break;
                    }
                    pos += got;
                }
            }
            else
            {
                for (int i = 0; i < b.Length; i++)
                    b[i] = 0;
            }
            System.Runtime.InteropServices.Marshal.Copy(b, 0, data, size);

        }

		public void OnWavCompleted(Boolean canceled)
        {
            SoundCompletedObserver.FireEvent(this, new SoundBufferCompletedEventArgs(canceled));          
       	}

	
        public Boolean Stop()
        {
            if (m_Player != null)
            {
                try
                {
                    m_Player.Dispose();
                   
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
                finally
                {
                    m_Player = null;
                }
            }
            return true;
        }

        public void Play()
        {
            if (m_AudioStream != null)
            {
               
                m_AudioStream.Position = 0;
                //Parameter 1, Default Device
                //Parameter 2, format of audio samples
                //Parameter 3 and 4 buffer sizes, 
                //m_Player = new WaveLib.WaveOutPlayer(-1, m_Format, 16384, 3, m_Buffer_Fill);
                m_Player = new WaveLib.WaveOutPlayer(-1, m_Format, 16384, 8, m_Buffer_Fill);
                m_Player.WavFinished += new WavFinishedDelegate(OnWavCompleted);
            }   
		}

        private void CloseStream()
        {
            Stop();
            if (m_AudioStream != null)
                try
                {
                    m_AudioStream.Close();
                }
                finally
                {
                    m_AudioStream = null;
                }
        }

        public  Boolean Load(String pathToFile)
        {
           
            try
            {
                byte[] fileContents = System.IO.File.ReadAllBytes(pathToFile);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(fileContents, 0, fileContents.Length, false);
                WaveLib.WaveStream S = new WaveLib.WaveStream(ms);

                if (S.Length <= 0)
                    throw new Exception("Invalid WAV file");
                m_Format = S.Format;
                if (m_Format.wFormatTag != (short)WaveLib.WaveFormats.Pcm && m_Format.wFormatTag != (short)WaveLib.WaveFormats.Float)
                    throw new Exception("Only PCM files are supported");

                m_AudioStream = S;
                return true;
            }
            catch (System.IO.FileNotFoundException fileNotFound)
            {

                CloseStream();
                Console.WriteLine(fileNotFound);
				OnWavCompleted(false);
                //String fileName = System.IO.Path.GetFileName(pathToFile);
                //TTS.Engine.Play(SignalCenter.Common.Constants.WAV_FILE_NOT_FOUND + " Press enter to continue ", TTS.Configuration.Policies.Sentences, TTS.Configuration.Voices.Male, -2);
                //System.Windows.Forms.MessageBox.Show(SignalCenter.Common.Constants.WAV_FILE_NOT_FOUND + " File not found at " + pathToFile, " Unable to locate WAV", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly, false);
                
                throw;
            }
            catch (Exception e)
            {
                CloseStream();
                Console.WriteLine(e);
				OnWavCompleted(false);
                throw;
            }
        }
       



       
    }
}
