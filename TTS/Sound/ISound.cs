﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignalCenter.TTS.Sound
{
   public interface ISound
    {


        Boolean IsPlaying{ get; }

		void AddSubscriber(EventHandler subscriber);

        Boolean Stop();
       
        void Play();

        Boolean Load(String filePath);

		void OnWavCompleted(Boolean canceled);
    }
}
