﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using WaveLib;


namespace SignalCenter.TTS.Sound
{
    public class SoundUtilityComparer : IEqualityComparer<SoundUtility>
    {
        /// <summary>
        /// Checks Equality between two SoundUtility instances. Two instance are equal iff their UniqueIdentifier properties are equal.
        /// </summary>
        /// <param name="sound1"></param>
        /// <param name="sound2"></param>
        /// <returns></returns>
        public Boolean Equals(SoundUtility sound1, SoundUtility sound2)
        {
            if (sound1.Equals(sound2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the hashcode for the instance.
        /// </summary>
        /// <param name="sound"></param>
        /// <returns></returns>
        public int GetHashCode(SoundUtility sound)
        {
            int c = 41 * sound.UniqueIdentifier.GetHashCode();
            return c.GetHashCode();
        }
    }
}