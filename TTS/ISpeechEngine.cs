﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignalCenter.TTS
{
    public interface ISpeechEngine
    {
        /// <summary>
        /// Plays speech. The playing should not stop until all previous have stopped or been cancelled.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="mode"></param>
        /// <param name="voice"></param>
        /// <returns></returns>

        void Play(String text, Configuration.Policies mode, Configuration.Voices voice);

        /// <summary>
        /// Plays speech. The playing should not stop until all previous have stopped or been cancelled.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="mode"></param>
        /// <param name="voice"></param>
        /// <returns></returns>
        void Play(String text, Configuration.Policies mode);

        void Play(String text, Configuration.Policies mode, Configuration.Voices voice, int rate);

        /// <summary>
        /// Gets the valid voice names for this engine.
        /// </summary>
        /// <returns>The voices.</returns>
        List<String> GetVoices();

        /// <summary>
        /// Gets all the valid female voice names
        /// </summary>
        /// <returns></returns>
        List<String> GetFemaleVoices();

        /// <summary>
        /// Gets all the valid male voice names.
        /// </summary>
        /// <returns></returns>
        List<String> GetMaleVoices();

        /// <summary>
        /// Cancels all speech.
        /// </summary>
        void Cancel();

        /// <summary>
        /// Checks if speech is playing.
        /// </summary>
        bool IsPlaying();
    }
}
