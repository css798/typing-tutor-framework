﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignalCenter.TTS
{

    //  Handles Speech Synth
    public class SayEngine : ISpeechEngine
    {
        public SayEngine()
        {
            _Say = null;
        }

        /// <summary>
        /// Plays speech. The playing should not stop until all previous have stopped or been cancelled.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="mode"></param>
        /// <param name="voice"></param>
        /// <returns></returns>
        public void Play(String text, Configuration.Policies mode, Configuration.Voices voice)
        {
            Play(text, mode, voice, SignalCenter.Common.UserManager.Instance.CurrentUser.PrefTTSRate);
        }
        /// <summary>
        /// Plays speech. The playing should not stop until all previous have stopped or been cancelled.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="mode"></param>
        /// <param name="voice"></param>
        /// <returns></returns>
        public void Play(String text, Configuration.Policies mode)
        {
            Play(text, mode, Configuration.Voices.Female, SignalCenter.Common.UserManager.Instance.CurrentUser.PrefTTSRate);
        }
        public void Play(String text, Configuration.Policies mode, Configuration.Voices voice, int rate)
        {
            if (mode == Configuration.Policies.Sentences || mode == Configuration.Policies.Words)
            {
                _Rate = rate;
                _Say = new System.Diagnostics.Process();

                _Say.StartInfo.FileName = "say";
                _Say.StartInfo.Arguments = "-v " + GetVoice(voice) + " -r " + _Rate + " " + Sanitize(text);
                _Say.StartInfo.CreateNoWindow = true;
                _Say.StartInfo.UseShellExecute = false;
                //	Console.WriteLine("Process Name: " + _Say.ProcessName  + "Thread Count: " + _Say.Threads.Count);
                _Say.Start();

            }

        }

        /// <summary>
        /// Cancels all speech.
        /// </summary>
        public void Cancel()
        {
            _Say.Kill();
            _Say = null;
        }

        /// <summary>
        /// Checks if speech is playing.
        /// </summary>
        public bool IsPlaying()
        {
            return !(_Say == null || _Say.HasExited);
        }

        private String GetVoice(Configuration.Voices voice)
        {
            switch (voice)
            {
                case SignalCenter.TTS.Configuration.Voices.Female:
                    return SignalCenter.Common.UserManager.Instance.CurrentUser.PrefFemaleVoice;
                case SignalCenter.TTS.Configuration.Voices.Male:
                    return SignalCenter.Common.UserManager.Instance.CurrentUser.PrefMaleVoice;
                default:
                    return SignalCenter.Common.UserManager.Instance.CurrentUser.PrefMaleVoice;

            }
        }

        /// <summary>
        /// Gets all the voices specified in the app.config with the key "MaleVoices".
        /// </summary>
        /// <returns></returns>
        public List<String> GetMaleVoices()
        {
            return System.Configuration.ConfigurationManager.AppSettings["MaleVoices"].ToString().Split(',').ToList<String>();
        }
        public List<String> GetVoices()
        {
            List<String> voices = new List<String>();
            voices.AddRange(GetMaleVoices());
            voices.AddRange(GetFemaleVoices());
            return voices;
        }

        /// <summary>
        /// Gets all the voices specefied in the app.config with the key "FemaleVoices".
        /// </summary>
        /// <returns></returns>
        public List<String> GetFemaleVoices()
        {
           return System.Configuration.ConfigurationManager.AppSettings["FemaleVoices"].ToString().Split(',').ToList<String>();
        }

        private String Sanitize(String text)
        {
            return "\"" + text.Replace("\"", "\\\"") + "\""; // Escape quote marks and enclose in quotes.
        }

        private System.Diagnostics.Process _Say;
        private int _Rate;

    }
    
}
