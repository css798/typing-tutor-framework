﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SignalCenter.Common;

namespace SignalCenter.Common
{
    /// <summary>
    /// Validates Keys and I/O readings for non printable character
    /// </summary>
    public static class KeyValidator
    {
       /// <summary>
       /// Validates User Input with Expected Character
       /// </summary>
       /// <param name="keyPressedArgs">User Input</param>
       /// <param name="expectedCharacter">Character to Validate Against</param>
       /// <param name="filePath">Stores the AbsoluteFilePath of the Translated Character, regardless of the Validation results</param>
       /// <returns>True, if the user input matches the expected character.</returns>
       internal static bool Validate(KeyEventArgs keyPressedArgs, Char expectedCharacter, out String filePath)
        {

            // Declare temporary variables.
            String translatedKeyCode = "";
            String translatedFirstCharacter = "";

            //Translate the Key Pressed Code and the Expected Character
            translatedKeyCode =KeyTranslator.Instance.GetFilePath(keyPressedArgs);
            translatedFirstCharacter = KeyTranslator.Instance.GetFilePath(expectedCharacter);
            filePath = translatedKeyCode;

            //Validates the Expected Character with the Character Pressed
            if (translatedFirstCharacter == translatedKeyCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private static Keys lastKeyCode;
        private static int shortcutKeyPressesFound = 0;

        /// <summary>
        /// Detects when enough keys have been pressed to satisfy a shortcut criteria.
        /// </summary>
        /// <param name="keyPressed">Latest key pressed</param>
        /// <returns></returns>
        public static SignalCenter.Common.Settings.Keyboard.ShortcutTypes DetectShortcut(KeyEventArgs keyPressed)
        {
            Keys currentKeyCode = keyPressed.KeyCode;

            //Handles Reset Functionality
            if (!currentKeyCode.Equals(lastKeyCode))
                shortcutKeyPressesFound = 0;
            
            lastKeyCode = currentKeyCode;

            //Each condition checks for a type of shortcut and if the condition to process it has been satisfied.
            if(currentKeyCode.Equals(Settings.Keyboard.HelpModeShortcut))
            {
                Settings.Keyboard.ShortcutTypes shortcut;
                if(inHelpMode)
                {
                    shortcut = Settings.Keyboard.ShortcutTypes.ExitHelpMode;
                }
                else
                {
                    shortcut = Settings.Keyboard.ShortcutTypes.EnterHelpMode;
                }
                inHelpMode = !inHelpMode;
                Console.WriteLine("Help Mode Changed to - " + inHelpMode);
                return shortcut;
            }
            else if (inHelpMode)
            {
                Console.WriteLine("In Help Mode");
                return Settings.Keyboard.ShortcutTypes.HelpMode;
            }
            else if (currentKeyCode.Equals(Settings.Keyboard.BackShortCut))
            {
                shortcutKeyPressesFound++;
                if (shortcutKeyPressesFound == 2)
                {
                    shortcutKeyPressesFound = 0;
                    return Settings.Keyboard.ShortcutTypes.Back;
                }
            }
            else if (Settings.Keyboard.ForwardShortCut.Contains<Keys>(currentKeyCode))
            {
                
               // shortcutKeyPressesFound++;
              //  if (shortcutKeyPressesFound == 2)
              //  {
                    shortcutKeyPressesFound = 0;
                    return Settings.Keyboard.ShortcutTypes.Forward;
               // }
            }
            else if (currentKeyCode.Equals(Settings.Keyboard.SynchronizationShortcut))
            {
                shortcutKeyPressesFound = 0;
                return Settings.Keyboard.ShortcutTypes.Synchronize;
            }
            else if (currentKeyCode.Equals(Settings.Keyboard.RepeatShortcut))
            {
                shortcutKeyPressesFound = 0;
                return Settings.Keyboard.ShortcutTypes.Repeat;
            }
            else if (currentKeyCode.Equals(Settings.Keyboard.HelpShortcut))
            {
                shortcutKeyPressesFound = 0;
                return Settings.Keyboard.ShortcutTypes.Help;
            }
            else if (Settings.Keyboard.IgnoreKeys.Equals(currentKeyCode))
            {
                shortcutKeyPressesFound++;
                if (shortcutKeyPressesFound == 2)
                {
                    shortcutKeyPressesFound = 0;
                    return Settings.Keyboard.ShortcutTypes.Ignore;
                }
            }
            return Settings.Keyboard.ShortcutTypes.None;
        }
        private static Boolean inHelpMode = false;
    }
}
