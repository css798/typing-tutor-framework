﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Xml.Serialization;

namespace SignalCenter.Common
{
    [Serializable]
    public class UserManager
    {
        #region Properties and Members

        /// <summary>
        /// Contains the list of Users which currently exist on the local machine
        /// </summary>
        public List<User> UserList { get; set; }

        /// <summary>
        /// Gets the Singleton Instance of the User Manager
        /// </summary>
        public static UserManager Instance {
            get
            {
                if (_INSTANCE == null)
                {
                    _INSTANCE = new UserManager();
                }

                return _INSTANCE;
                
            }
        }
        private static UserManager _INSTANCE;
        
        /// <summary>
        /// Gets the total available Users
        /// </summary>
        public static int UserCount { get; private set; }

        /// <summary>
        /// Contains the current User's User Instance
        /// </summary>
        public User CurrentUser { get; internal set; }
        
        #endregion

        #region Constructors

        /// <summary>
        /// Constructs the set of Users
        /// </summary>
        private UserManager()
        {
            UserList = new List<User>();
            UserList.Add(new User());
            CurrentUser = UserList.ElementAt<User>(0);

            try
            {
                this.RefreshUsers();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refreshes the Manager.  Clears all manager properties and reassigns them.
        /// If an exception occured while reading any User, then TTS plays the string specified in Constants.LESSON_PARSE_FAILURE.
        /// </summary>
        /// <returns></returns>
        public void RefreshUsers()
        {
            XmlSerializer mySerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<User>));
            StreamReader myReader = null;

            try
            {
                myReader = new StreamReader(SignalCenter.Common.Constants.XML_PATH);

                this.UserList = mySerializer.Deserialize(myReader) as List<User>;
                
                SetCurrentUser(CurrentUser.UserName);
            }
            catch (FileNotFoundException e2)
            {
                File.Create(SignalCenter.Common.Constants.XML_PATH).Close();

                this.SaveUsers();
            }
            catch (Exception e)
            {
               // TTS.Engine.Play(Constants.SYNCHRONIZATION_FAILURE, TTS.Configuration.Policies.Sentences);
            }
            finally
            {
                if (myReader != null)
                    myReader.Close();
            } 
        }

        /// <summary>
        /// Sets the Current User given the disired User Name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public Boolean SetCurrentUser(String userName)
        {
            Boolean results = false;
            //Allows for updating the current user after the user list has been updated.
            foreach (User aUser in UserList)
            {
                if (aUser.UserName.Equals(userName))
                {
                    this.CurrentUser = aUser;
                    results = true;
                    break;
                }
            }

            return results;
        }

        /// <summary>
        /// Add a new user to the UserList
        /// Enforces uniqueness
        /// </summary>
        /// <param name="newUser"></param>
        public bool Add(User newUser)
        {
            if (!this.Contains(newUser.UserName))
            {
                UserList.Add(newUser);
                return true;
            }
            else
                return false;
        }

        public bool Remove(User user)
        {
            this.UserList.Remove(user);
            return true;
        }

        /// <summary>
        /// Returns boolean denoting the presents of user in the list based on username.
        /// If true then user already exists.
        /// If false then user does not exist.
        /// </summary>
        /// <param name="uname"></param>
        /// <returns></returns>
        public bool Contains(string uname)
        {
            bool rval = false;
            
            foreach (User user in UserList)
            {
                if (user.UserName == uname)
                    rval = true;
            }

            return rval;
        }

        /// <summary>
        /// Saves the User List
        /// </summary>
        public void SaveUsers()
        {
            XmlSerializer mySerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<User>));

            using (FileStream fs = new FileStream(SignalCenter.Common.Constants.XML_PATH, FileMode.Create, FileAccess.ReadWrite))
            {
                using (StreamWriter myWriter = new StreamWriter(fs))//= new StreamWriter(SignalCenter.Common.Constants.XML_PATH);
                {
                    try
                    {
                        mySerializer.Serialize(myWriter, UserManager.Instance.UserList);
                    }
                    catch (Exception e)
                    {
                        Console.Write(e);
                        TTS.Engine.Play("Unable to save users", TTS.Configuration.Policies.Sentences, TTS.Configuration.Voices.Male);
                    }
                }
            }
        }

        /// <summary>
        /// Saves the User List
        /// </summary>
        public void SaveUsers(string path)
        {
            XmlSerializer mySerializer = new System.Xml.Serialization.XmlSerializer(typeof(List<User>));
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Create))
                {
                    using (StreamWriter myWriter = new StreamWriter(fs))
                    {
                        mySerializer.Serialize(myWriter, UserManager.Instance.UserList);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to save users");
            }
        }

        #endregion
    }
}
    namespace SignalCenter.Common.Util
    {

        public static class UserManagerExtensions
        {
            /// <summary>
            /// Recursively, sets the backcolor, forecolor, and font size for all controls located in this control's control collection.
            /// </summary>
            /// <param name="c"></param>
            public static void SynchronizeControl(this Control c)
            {
                c.BackColor = Color.FromName(UserManager.Instance.CurrentUser.PrefBackColor);
                c.ForeColor = Color.FromName(UserManager.Instance.CurrentUser.PrefFontColor);
                c.Font = new Font(UserManager.Instance.CurrentUser.PrefFontName, UserManager.Instance.CurrentUser.PrefFontSize);
                c.Invalidate();

                foreach (Control ac in c.Controls)
                {
                    ac.SynchronizeControl();
                }
            }
        }

    }