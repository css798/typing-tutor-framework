﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Drawing;
using SignalCenter.Common;
using SignalCenter.Lessons;
using System.Xml.Serialization;


namespace SignalCenter.Common
{
    [Serializable]
    public class User : INotifyPropertyChanged
    {
        
        #region Properties and Members

        #region User Properties

        /// <summary>
        /// Contains the User's username
        /// Default = <code>"Guest"</code>
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public string UserName 
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
                OnChanged("UserName");
            }
        }
        private string _UserName;

        /// <summary>
        /// Cotains the User's first name
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public string FirstName 
        {
            get
            {
                return _FirstName; 
            }
            set
            {
                _FirstName = value;
                OnChanged("FirstName");
            }
        }
        private string _FirstName;

        /// <summary>
        /// Cotains the User's first name
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
                OnChanged("LastName");
            }
        }
        private string _LastName;

        /// <summary>
        /// Contains the User's current lesson number
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public int CurrentLesson {
            get
            {
                return _CurrentLesson;
            }
            set
            {
                _CurrentLesson = value;
                OnChanged("CurrentLesson");
            }
        }
        private int _CurrentLesson;

        #endregion

        #region Preference Settings

        /// <summary>
        /// Contains the TTS speach rate setting
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public int PrefTTSRate
        {
            get
            {
                return _PrefTTSRate;
            }
            set
            {
                _PrefTTSRate = value;
                OnChanged("PrefTTSRate");
            }
        }
        private int _PrefTTSRate;

        /// <summary>
        /// Contains the TTS Male voice selection setting
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public string PrefMaleVoice 
        {
            get
            {
                return _PrefMaleVoice;
            }
            set
            {
                _PrefMaleVoice = value;
                OnChanged("PrefMaleVoice");
            }
        }
        private string _PrefMaleVoice;

        /// <summary>
        /// Contains the TTS Female voice selection setting
        /// </summary>
        public string PrefFemaleVoice 
        {
            get
            {
                return _PrefFemaleVoice;
            }
            set
            {
                _PrefFemaleVoice = value;
                OnChanged("PrefFemaleVoice");
            }
        }
        private string _PrefFemaleVoice;

        /// <summary>
        /// Contains the font size setting
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public Single PrefFontSize 
        {
            get
            {
                return _PrefFontSize;
            }
            set
            {
                _PrefFontSize = value;
                OnChanged("PrefFontSize");
            }
        }
        private Single _PrefFontSize;
            
        /// <summary>
        /// Contains the Name of the font prefered
        /// </summary>
        public string PrefFontName 
        {
            get
            {
                return _PrefFontName;
            }
            set
            {
                _PrefFontName = value;
                OnChanged("PrefFontName");
            }
        }
        private string _PrefFontName;

        /// <summary>
        /// Contains the font color setting as a string
        /// Used for XML Serialization and Bindings.
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        [XmlElement("PrefFontColor")]
        public string PrefFontColor
        {
            get
            {
                return _PrefFontColor;
            }
            set
            {
                _PrefFontColor = value;
                OnChanged("PrefFontColor");
            }
        }
        private string _PrefFontColor;

        /// <summary>
        /// Contains the font color setting as a string
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        [XmlElement("PrefBackgroundColor")]
        public string PrefBackColor
        {
            get
            {
                return _PrefBackColor;
            }
            set
            {
                _PrefBackColor = value;
                OnChanged("PrefBackColor");              
            }
        }
        private string _PrefBackColor;

        /// <summary>
        /// Contains the boolean that controls wether or not characters are enunciated during lesson
        /// if true then characters are enunciated
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public bool PrefCharToggle 
        {
            get
            {
                return _PrefCharToggle;
            }
            set
            {
                _PrefCharToggle = value;
                OnChanged("PrefCharToggle");
            }
        }
        private bool _PrefCharToggle;

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public User()
            : this("Guest User", "Guest", "User", 0, 1, TTS.Engine.GetVoices()[0], TTS.Engine.GetVoices()[0], 150, "Consolas", Color.White, Color.Black, Constants.PLAY_VALID_KEY_PRESS)
        {

        }

        /// <summary>
        /// Constructs a User
        /// Allows custom values for all properties in class
        /// Main entry point for all other constructors
        /// </summary>
        /// <param name="uName">Sets the user's username</param>
        /// <param name="fName">Sets the user's first name</param>
        /// <param name="rate">Sets the TTS speach rate setting</param>
        /// <param name="mVoice">Sets the TTS Male voice selection setting</param>
        /// <param name="fVoice">Sets the TTS Female voice selection setting</param>
        /// <param name="size">Sets the font size setting</param>
        /// <param name="fontName">Sets the font name setting</param>
        /// <param name="fColor">Sets the font color setting</param>
        /// <param name="bColor">Sets the background color setting</param>
        /// <param name="cTog">Sets the character enunciation setting</param>
        public User(string uName, string fName, string lName, int curLesson, int rate, string mVoice, string fVoice, int size, string fontName, Color fColor, Color bColor, bool cTog)
        {
            UserName = uName;
            FirstName = fName;
            LastName = lName;
            CurrentLesson = curLesson;
            
			if(!SignalCenter.Common.Constants.IsWindows && rate < 100){
            	PrefTTSRate = 200;
			}else{
				PrefTTSRate = rate;
			}
            PrefMaleVoice = mVoice;
            PrefFemaleVoice = fVoice;
            PrefFontSize = size;
            PrefFontName = fontName;
            PrefFontColor = fColor.Name;
            PrefBackColor = bColor.Name;
            PrefCharToggle = cTog;
        }


        #endregion

        #region Methods

        #region Pulic Methods

        public override string ToString()
        {
            return UserName;
        }

        #endregion

        #region Private Methods

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Fires the INotifyPropertyChanged event.
        /// </summary>
        /// <param name="caller"></param>
        private void OnChanged(string caller = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(caller));
            }
        }

        #endregion

        #endregion
    }

    public class UserEqualityComparer : IEqualityComparer<User>
    {
        public bool Equals(User u1, User u2)
        {
            if (u1.FirstName == u2.FirstName && u1.LastName == u2.LastName)
                return true;
            else
                return false;            
        }

        public int GetHashCode(User user)
        {
            int c = 47*user.UserName.GetHashCode();
            return c.GetHashCode();
        }

    }
}
