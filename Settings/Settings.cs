﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SignalCenter.Common;
using System.Configuration;
using System.Windows.Forms;

namespace SignalCenter.Common.Settings
{
    /// <summary>
    /// Provides all important keyboard keys and there functions.
    /// </summary>
    public static class Keyboard
    {
        public static Keys BackShortCut { get { return Keys.Escape;  } }
        public static Keys[] ForwardShortCut { get { return new Keys[] {Keys.Enter, Keys.Space }; } }
        public static Keys IgnoreKeys { get { return Keys.ControlKey; } }
        public static Keys HelpShortcut { get { return Keys.F1; } }
        public static Keys RepeatShortcut { get { return Keys.F4; } }
        public static Keys SynchronizationShortcut { get { return Keys.F8; } }
        public static Keys HelpModeShortcut { get { return Keys.F12; } }

        /// <summary>
        /// Defines all types of Keyboard Shortcuts.
        /// </summary>
        public enum ShortcutTypes
        {
            Back,
            Forward,
            Ignore,
            Home,
            None,
            Repeat,
            Help,
            HelpMode,
            EnterHelpMode,
            ExitHelpMode,
            Synchronize
        };
    }
}

