
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Text.RegularExpressions;
using System.Linq.Expressions;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Text;
using SignalCenter.TTS.Configuration;

namespace SignalCenter.Common.Util
{

    public  static class LessonExtensions
    {
     

        /// <summary>
        /// Randomizes a collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The collection to randomize</param>
        /// <param name="randomizeAfter">Allows for ensuring order from the this value and before in the collection. This is exclusively bound.</param>
        /// <returns></returns>
        public static  List<T> Randomize<T>(this List<T> list, int randomizeAfter)
        {
            List<T> randomizedList = new System.Collections.Generic.List<T>();
            Random rnd = new Random();

            if (randomizeAfter >= list.Count)
            {
                randomizeAfter = list.Count - 1;
            }

            //Ensuring Order up to the randomizeAfter.
            for (int i = 0; i < randomizeAfter; i++)
            {
                Console.WriteLine("DEBUG: " + list[0]);
                randomizedList.Add(list[0]);
                list.RemoveAt(0);
            }

            while (list.Count > 0)
            {
                int index = rnd.Next(0, list.Count); //pick a random item from the master list
                index = rnd.Next(0, list.Count);
                randomizedList.Add(list[index]); //place it at the end of the randomized list
                list.RemoveAt(index);
            }

            
            return randomizedList;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="grammarItems"></param>
        /// <param name="elementIndex"> The index within the from index to start from</param>
        /// <param name="fromIndex"> Starting index, such that fromIndex is the index to start from.</param>
        /// <param name="toIndex">To index, non-inclusively. (I.E. i less than toIndex) </param>
        /// <returns></returns>
        public static String GetSegment(this IList<String> grammarItems, int elementIndex, int fromIndex, int toIndex)
        {
            if (toIndex > grammarItems.Count())
            {
                toIndex = grammarItems.Count() - 1;
            }

            if (elementIndex != 0)
            {
                int temp = elementIndex;
                if (elementIndex >= grammarItems[fromIndex].Length)
                {
                    elementIndex = grammarItems[fromIndex].Length - 1;
                }

                //Check for the last occurence of space which signifies the beginning of the current word.
                temp = grammarItems[fromIndex].LastIndexOf(" ", elementIndex);
              
                //Check for the existance of the last space.
                elementIndex = (temp == -1) ? 0 : temp;
            }

            StringBuilder builder = new StringBuilder();
            builder.Append(grammarItems[fromIndex].Substring(elementIndex));
            for (int i = ++fromIndex; i < toIndex; i++)
            {
                builder.Append(grammarItems[i]);
            }

            return builder.ToString();
        }

        public static List<String> InitializeGrammarList(this String lessonContent, TTS.Configuration.Policies speechPolicy)
        {
            
            if (speechPolicy.Equals(Policies.Words))
            {
                return lessonContent.ToWordList();
            }
            else if (speechPolicy.Equals(Policies.Sentences))
            {
                return lessonContent.ToSentenceList();
            }
            else
            {
                return lessonContent.ToCharacterList();
            }
        }
      
        
        /// <summary>
        /// This will take any string and parse out all the words in it.
        /// </summary>
        /// <param name="lessonContent"></param>
        /// <returns>A list of words</returns>
        public static List<String> ToWordList(this string lessonContent)
        {

          //  Regex rx = new Regex(@".*(?= [\w]+)");
          //  List<String> matches = Regex.Split(lessonContent, @"\s",RegexOptions.IgnorePatternWhitespace).ToList<String>();
          //  List<String> adjustedMatched = new List<string>();
           // foreach (String s in matches)
          //  {
          //      adjustedMatched.Add(s + " ");
          //  }
            List<String> matches = lessonContent.Split(new char[] { ' ' }, StringSplitOptions.None).ToList<String>();
            return matches;
        }

        public static List<String> ToSentenceList(this string lessonContent)
        {

            Regex rx = new Regex(@"(?<=['""A-Za-z0-9][\.\!\?])\s+(?=[A-Z])");
            List<String> matches = rx.Split(lessonContent).ToList<String>();
            return matches;
        }
        
        
        public static List<String> ToCharacterList(this string lessonContent)
        {

            Regex rx = new Regex(@"(.)");
            MatchCollection matches = rx.Matches(lessonContent);
            List<String> matchesList = new List<String>();
            foreach(Match match in matches)
                matchesList.Add(match.Value);
            return matchesList;
        }
    }
	public static class IOBuilder
	{
        public static String BuildProgramDataDirectory()
        {
			//if(Constants.IsWindows){
           //		 String systemProgramData = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
          //  	 return System.IO.Path.Combine(systemProgramData, "UTC", "Accessible Typing Tutor Software");
		//	}else{
				return System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
		//	}
        }
        /// <summary>
        /// Builds the file path to a given file based on the Voice type.
        /// </summary>
        /// <param name="voice">Voice type that will be speaking the WAV.</param>
        /// <param name="filePreFix">Allows for specifing a base file path. This will be inserted at the beginning of the file path before the file path specified in the voices.</param>
        /// <param name="pathPostFix">This parameter contains the file name and any additionally sub directories after the voice type has been converted to a file path.</param>
        /// <returns>null if pathPostFix is empty, null, or a white space.</returns>
        public static String GetFilePath(this TTS.Configuration.Voices voice, String filePreFix, String pathPostFix)
        {
            //if (String.IsNullOrWhiteSpace(pathPostFix))
            //    return String.Empty;

            String baseFilePath = "";
            if (!voice.Equals(TTS.Configuration.Voices.Informative))
            {
                object temp = ConfigurationManager.AppSettings["TTSDirectory"];
                temp = (temp == null) ? String.Empty : temp;
                baseFilePath = System.IO.Path.Combine(temp.ToString().Split(','));
            }
            else if (voice.Equals(TTS.Configuration.Voices.Informative))
            {
                 object temp = ConfigurationManager.AppSettings["InformativeDirectory"];
                temp = (temp == null) ? String.Empty : temp;
                baseFilePath = System.IO.Path.Combine(temp.ToString().Split(','));
            }
            string filePath = System.IO.Path.Combine(filePreFix, baseFilePath, voice.ToString(), pathPostFix);
			filePath = System.IO.Path.Combine(BuildProgramDataDirectory(), filePath);
			Console.WriteLine ("GetFilePath for Voice - " + filePath);
			return filePath;
        }


		/// <summary>
		/// Builds the wav path. 
		/// </summary>
		/// <returns>The wav file path.</returns>
		/// <param name="lesson">The lesson number</param>
		/// <param name="fileName">Name of wav file path.</param>
		public static String BuildLessonAudioPath (int lesson, string fileName)
		{
            return System.IO.Path.Combine(BuildDirectoryPath(lesson), fileName);
		}

        public static String BuildLessonDirectoryPath()
        {
            String lessonDirectoryPath = ConfigurationManager.AppSettings["LessonDirectory"].ToString();
            return System.IO.Path.Combine(BuildProgramDataDirectory(), lessonDirectoryPath);
        }
		/// <summary>
		/// Dynamically Builds the path to the Directory Specified by the desired lesson number.
		/// </summary>
		/// <param name="lessonNumber"></param>
		/// <returns></returns>
		public static String BuildDirectoryPath(int lessonNumber)
		{
			return System.IO.Path.Combine(BuildLessonDirectoryPath(), lessonNumber.ToString());
		}

        public static String BuildVoiceDirectoryPath(TTS.Configuration.Voices voice)
        {

		 return System.IO.Path.Combine(IOBuilder.BuildProgramDataDirectory(), voice.GetFilePath(String.Empty,String.Empty));
        }
		/// <summary>
		/// Dynamically Builds the AbsoluteFilePath based on the AppConfig Values.
		/// </summary>
		/// <returns></returns>
		public static String BuildFileName()
		{
			String baseLessonName = ConfigurationManager.AppSettings["BaseFileName"].ToString();
			String fileExtension = ConfigurationManager.AppSettings["FileExtension"].ToString();
			
			if (!fileExtension.StartsWith("."))
				fileExtension = "." + fileExtension;
			
			return baseLessonName + fileExtension;
		}
		
		/// <summary>
		/// Dynamically Builds the Path to the File specified by the Lesson Number
		/// </summary>
		/// <param name="lessonNumber"></param>
		/// <returns></returns>
        public static String BuildFilePath(int lessonNumber)
        {
            return System.IO.Path.Combine(BuildDirectoryPath(lessonNumber), BuildFileName());
        }

    }
    public class SettingsExtension
    {
        public static String TrySettingOrDefault(Object source, String sdefault)
        {
            try
            {
                return source.ToString();
            }
            catch (Exception e)
            {
                return sdefault;
            }

        }
    }

  
   
}

