﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
namespace SignalCenter.Common.Util
{
    public class XmlBuilder
    {
        private static readonly string FILE_LOCATION = Constants.DATALOGGING_XML_PATH;
        private XmlDocument xmlCompletionData;

        private static XmlBuilder INSTANCE = new XmlBuilder();

        public static Boolean FindLastLesson(String username, int lessonNumber, out SignalCenter.Lessons.LessonStatistics results)
        {
            if (INSTANCE.xmlCompletionData == null)
            {
                INSTANCE.xmlCompletionData = XmlBuilder.INSTANCE.ConstructXmlDocument();
            }
         
            XmlNodeList xnList = INSTANCE.xmlCompletionData.SelectNodes("/Feed/Row");
            SignalCenter.Lessons.LessonStatistics stats = new SignalCenter.Lessons.LessonStatistics(lessonNumber);
            Boolean returnresults = false;
            foreach (XmlNode xn in xnList)
            {
                if (xn["User"].InnerText.Equals(username) && Convert.ToInt32(xn["LessonNumber"].InnerText).Equals(lessonNumber))
                {
                    Console.WriteLine("User" + username);
                    returnresults = true;
                    stats = new SignalCenter.Lessons.LessonStatistics(lessonNumber, Convert.ToInt32(xn["Errors"].InnerText),Convert.ToDecimal(xn["WPM"].InnerText), Convert.ToDecimal(xn["CPM"].InnerText), Convert.ToDecimal(xn["Accuracy"].InnerText), new TimeSpan());
					stats = SignalCenter.Lessons.LessonStatistics.RoundAll(stats);
					break;
                }

            }
            results = stats;
            return returnresults;


        }

        public static bool AddRecord(SignalCenter.Lessons.LessonStatistics stats, String userName)
        {
            Boolean results = false;
            try
            {
                XmlDocument xmlDoc = INSTANCE.ConstructXmlDocument();
                XmlNode rootNode = INSTANCE.GetFirstChild(xmlDoc);
                XmlNode rowNode = INSTANCE.ConstructRowNode(xmlDoc, stats, userName);

                rootNode.InsertBefore(rowNode, rootNode["Row"]);
                xmlDoc.Save(FILE_LOCATION);
                INSTANCE.xmlCompletionData = xmlDoc;
                results = true;   
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return results;
        }

        private XmlNode GetFirstChild(XmlDocument xmlDoc)
        {
            XmlNode rootNode = null;
            if (xmlDoc.FirstChild.NextSibling == null)
            {
                rootNode = xmlDoc.CreateElement("Feed");
                xmlDoc.FirstChild.AppendChild(rootNode);
            }
            else if (!xmlDoc.FirstChild.NextSibling.Name.Equals("Feed"))
            {
                rootNode = xmlDoc.CreateElement("Feed");
                xmlDoc.InsertBefore(rootNode, xmlDoc.FirstChild.NextSibling);
            }
            else
            {
                rootNode = xmlDoc.FirstChild.NextSibling;
            }
            return rootNode;


        }
        private  XmlNode ConstructRowNode(XmlDocument xmlDoc, SignalCenter.Lessons.LessonStatistics stats, String userName)
        {

            XmlNode rowNode = xmlDoc.CreateElement("Row");
            XmlNode createBy = xmlDoc.CreateElement("Created-By");
            createBy.InnerText = "Application";
            rowNode.AppendChild(createBy);

            XmlNode dateTimeNode = xmlDoc.CreateElement("Created-On");
            dateTimeNode.InnerText = DateTime.Now.ToString("d/M/yyyy HH:mm:ss");
            rowNode.AppendChild(dateTimeNode);

            XmlNode userNode = xmlDoc.CreateElement("User");
            userNode.InnerText = (String.IsNullOrEmpty(userName) ? "Not Specified" : userName);
            rowNode.AppendChild(userNode);

            XmlNode lessonNumberNode = xmlDoc.CreateElement("LessonNumber");
            lessonNumberNode.InnerText = stats.LessonNumber.ToString();
            rowNode.AppendChild(lessonNumberNode);

            XmlNode wpmNode = xmlDoc.CreateElement("WPM");
            wpmNode.InnerText = stats.WordsPerMinute.ToString();
            rowNode.AppendChild(wpmNode);

            XmlNode cpmNode = xmlDoc.CreateElement("CPM");
            cpmNode.InnerText = stats.KeysPerMinute.ToString();
            rowNode.AppendChild(cpmNode);

            XmlNode accuracyNode = xmlDoc.CreateElement("Accuracy");
            accuracyNode.InnerText = stats.Accuracy.ToString();
            rowNode.AppendChild(accuracyNode);

            XmlNode errorsNode = xmlDoc.CreateElement("Errors");
            errorsNode.InnerText = stats.Errors.ToString();
            rowNode.AppendChild(errorsNode);

            XmlNode durationNode = xmlDoc.CreateElement("Duration");
            durationNode.InnerText = stats.ElapsedTime.ToString(@"hh\:mm\:ss"); ;
            rowNode.AppendChild(durationNode);

            return rowNode;
        }
        private XmlDocument ConstructXmlDocument()
        {
            XmlDocument xmlDoc = new XmlDocument();
            String xmlContent = String.Empty;
            try
            {
                xmlContent = System.IO.File.ReadAllText(FILE_LOCATION);
                xmlDoc.LoadXml(xmlContent);
            }
            catch (System.IO.FileNotFoundException e2)
            {
                System.IO.File.Create(FILE_LOCATION).Close();
                ConstructXmlDocument();
            }
            catch (Exception e)
            {
                Console.Write(e);
                TTS.Engine.Play("Unable to save lesson statistics",TTS.Configuration.Policies.Sentences,TTS.Configuration.Voices.Male);
            }

            return xmlDoc;

        }
    }
}