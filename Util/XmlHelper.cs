﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace SignalCenter.Common
{
    public class XmlHelper
    {
        public XmlHelper() { }

        public static T DeserializeFromXml<T>(string filePath)
        {
            T t = default(T);
            System.IO.FileStream fileStream = null;
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                fileStream = new FileStream(filePath, FileMode.Open);
                t = (T)xmlSerializer.Deserialize(fileStream);
            }
            catch (Exception e)
            {
                string strErrMsg = "ERROR: XmlHelper: DeserializeFromXml<T>(): " + e.Message;
                WriteMessageToErrorLog(strErrMsg); ;
            }
            finally
            {
                if (fileStream != null)
                {
                    fileStream.Close();
                }
            }
            return (t);
        }

        public static bool SerializeToXml<T>(T t, string filePath)
        {
            bool result = true;
            FileStream fileStream = null;
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                fileStream = new FileStream(filePath, FileMode.Create);
                xmlSerializer.Serialize(fileStream, t);
            }
            catch (Exception e)
            {
                string strErrMsg = "ERROR: XmlHelperUtilities: SerializeToXml<T>(): " + e.Message;
                WriteMessageToErrorLog(strErrMsg);
                result = false;
            }
            finally
            {
                if (fileStream != null)
                {
                    fileStream.Close();
                }
            }
            return (result);
        }
        
        /// <summary>
        /// Deep copies T.
        /// </summary>
        public static T Copy<T>(T t)
        {
            T copy = default(T);
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                // Write the settings document to memory stream.
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, t);

                memoryStream.Position = 0;
                copy = (T)binaryFormatter.Deserialize(memoryStream);
                // Explicitly close the MemoryStream object to release its memory.
                memoryStream.Close();
            }
            catch (Exception ex)
            {
                string strErrMsg = "ERROR: XmlHelper: Copy<T>(T t): " + ex.Message;
                WriteMessageToErrorLog(strErrMsg);
            }
            return (copy);
        }
        
        public static void WriteMessageToErrorLog(string errorMessage)
        {
            string pathToErrorLog = System.IO.Path.Combine("", "error_log.txt");
            WriteMessageToTextFile(errorMessage, pathToErrorLog);
        }

        public static void WriteMessageToTextFile(string messageToWrite, string filePathToDocument)
        {
            FileStream fileStream;
            StreamWriter streamWriter;
            try
            {
                fileStream = new FileStream(filePathToDocument, FileMode.OpenOrCreate);
                streamWriter = new StreamWriter(fileStream);
                // NOTE: Writing a char array rather than a string
                // avoids a bug with the WriteLine overload that uses a string,
                // whereby the character '{' causes an error!!!!!!!!!
                char[] msg = messageToWrite.ToCharArray();
                fileStream.Seek(0, System.IO.SeekOrigin.End);
                streamWriter.WriteLine(msg);
                streamWriter.Flush();
                fileStream.Flush();
                streamWriter.Close();
                fileStream.Close();
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message;
            }
        }

       /* public static string GetDirectoryOfApplication()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string assemblyName = assembly.FullName;
            string strDirectoryOfApplication = System.IO.Path.GetDirectoryName(assembly.Location);
            return (strDirectoryOfApplication);
        }*/
    }
}
