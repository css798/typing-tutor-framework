﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SignalCenter.Lessons;

namespace SignalCenter.Lessons
{
    /// <summary>
    /// Provides a comparer for the lesson class. This comparer checks the lesson number property.
    /// </summary>
    public class LessonComparer : IComparer<Lesson>
    {
        int results = 0;
        public int Compare(Lesson x1, Lesson x2)
        {
            if (x1.Number == x2.Number)
            {
                results = 0;
            }
            else if (x1.Number > x2.Number)
            {

                results = 1;
            }
            else if (x1.Number < x2.Number)
            {
                results = -1;
            }


            return results;
        }

    }

}
