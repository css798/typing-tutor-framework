
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;

namespace SignalCenter.Lessons
{
	public interface IKeyValidator
	{
        /// <summary>
        /// Member specifies when a valid key down has been pressed.
        /// </summary>
        /// <param name="fileName"></param>
		void OnValidKeyDown(string fileName);


        /// <summary>
        /// Member specifies when a invalid key down has been pressed.
        /// </summary>
        /// <param name="fileName"></param>
		void OnInvalidKeyDown(string fileName);
	}
}

