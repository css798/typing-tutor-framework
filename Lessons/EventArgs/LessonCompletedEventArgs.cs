﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignalCenter.Lessons.EventArgs
{

    /// <summary>
    /// Provides Arguments for the LessonCompletedEventHandler used in the LessonScreen class.
    /// </summary>
    public class LessonCompletedEventArgs : System.EventArgs
    {
        /// <summary>
        /// Gets the Lesson Statistics Associated with this attempt.
        /// </summary>
        public Lessons.LessonStatistics Statistics { get; private set; }

        /// <summary>
        /// Gets the status of the completed lesson.
        /// </summary>
        public Boolean IsCompleted { get; private set; }

        internal LessonCompletedEventArgs(Lessons.LessonStatistics stats, Boolean isCompletedLesson)
        {
            Statistics = stats;
            IsCompleted = isCompletedLesson;

        }
    }
}
