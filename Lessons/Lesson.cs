﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SignalCenter.Common.Util;
using System.ComponentModel;
using SignalCenter.Lessons.EventArgs;


namespace SignalCenter.Lessons
{
    public delegate void LessonCompletedEventHandler(object sender, LessonCompletedEventArgs e);
    public delegate void  LessonSegmentChangedEventHandler(object sender, LessonSegmentEventArgs e);
    public delegate void GrammarItemCompletedEventHandler(object sender, GrammarItemCompletedEventArgs e);

    /// <summary>
    /// Represents a lesson entity that is pulled from I/O using the LessonManager. 
    /// </summary>
    public class Lesson : INotifyPropertyChanged
    {
       #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
       public Lesson()
        {
            ContentNode node = new ContentNode();
            node.FileName    = ConfigurationManager.AppSettings["MasterySkills"].ToString();
			_MasterySkills    = node;
            node.FileName    = ConfigurationManager.AppSettings["Instructions"].ToString();
			_Instructions     = node;
            node.FileName    = ConfigurationManager.AppSettings["RequiredSkills"].ToString();
			_SkillsRequired   = node;
            
            lessonStats = new LessonStatistics(0);
			
        }

        #endregion

       #region Properties and Members

       #region Event Handler
           /// <summary>
           /// Gets and Sets the handler for when the lesson is completed.
           /// </summary>
       public LessonCompletedEventHandler      LessonCompleted      { get;   set;    }
       public LessonSegmentChangedEventHandler SegmentChanged       { get;   set;    }
       public GrammarItemCompletedEventHandler GrammarItemCompleted { get;   set;    }

       #endregion

       #region Protected Members

            protected  const int NUMBER_OF_ORDERED_SETS = 4;

            protected int randomizeAfterIndex;
            /// <summary>
            /// Represents the amount of elements at the beginning of any given segment from the grammar list.
            /// </summary>
            protected int elementsInSet;

            /// <summary>
            /// Represents the current element in the grammar list. 
            /// </summary>
            protected int currentGrammarItemIndex;

            /// <summary>
            /// Represents the current index within the string of grammarlist[currentGrammarItemIndex]
            /// </summary>
            protected int currentCharacterIndex;
            
            /// <summary>
            /// Represents the index of the last completed grammar item. 
            /// </summary>
            protected int lastCompletedIndex;

            protected int endOfGrammarSegmentIndex = 0;
            protected List<String> grammarList;
          
           #endregion
       
       #region Public Properties

        public event PropertyChangedEventHandler PropertyChanged;
        
        /// <summary>
		/// Used for handling where in the lesson data field the content property is currently.
		/// </summary>
        public TTS.Configuration.Policies SpeechPolicy { get { return _SpeechPolicy; }
             set
             {
                 _SpeechPolicy = value;
                 OnChanged("SpeechPolicy");

                 if (value == TTS.Configuration.Policies.Letters)
                 {
                     randomizeAfterIndex = NUMBER_OF_ORDERED_SETS * CHARACTERS_IN_SEGMENT;
                 }
                 else if(value == TTS.Configuration.Policies.Sentences)
                 {
                     randomizeAfterIndex = NUMBER_OF_ORDERED_SETS * SENTENCE_IN_SEGMENT;
                 }
                 else if (value == TTS.Configuration.Policies.Words)
                 {
                     randomizeAfterIndex = NUMBER_OF_ORDERED_SETS * WORDS_IN_SEGMENT;
                 }

             }
         }
        private TTS.Configuration.Policies _SpeechPolicy;

        /// <summary>
        /// Contains an absolute path to the mastery skills audio.
        /// </summary>
        public String MasterySkillAudioPath
        {
            get
            {
                return IOBuilder.BuildLessonAudioPath(Number, _MasterySkills.FileName);
            }
        }

        /// <summary>
        /// Contains an absolute path to the skills required audio.
        /// </summary>
        public String SkillsRequiredAudioPath
        {
            get
            {
                return IOBuilder.BuildLessonAudioPath(Number, _SkillsRequired.FileName);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public String InstructionsAudioPath
        {
            get
            {
                return IOBuilder.BuildLessonAudioPath(Number, _Instructions.FileName);
            }
        }

        [Bindable(BindableSupport.Yes)]
        public String MasterySkills 
        {
            get
            {
                return _MasterySkills.Content;
            }
            set
            {
                _MasterySkills.Content = value;
                OnChanged("MasterySkills");
            }
        }
        private ContentNode _MasterySkills;

        /// <summary>
        /// Gets and Sets the Instructions Fields of the Lesson.
        /// This property is used for
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public String Instructions
        {
            get
            {
                return _Instructions.Content;
            }
            set
            {
                _Instructions.Content = value;
                OnChanged("Instructions");
            }

        }
        private ContentNode _Instructions;

        ///// <summary>
        ///// Gets and Sets the Description Field of the Lesson.
        ///// This property is used for describing the lesson's content.  It may be useful to think of it as a title.
        ///// </summary>
        //[Bindable(BindableSupport.Yes)]
        //public String  TitleFileName{
        //    get
        //    {
        //      return _Description.FileName;
        //    }
        //    set
        //    {
        //        _Description.FileName = value;
        //        OnChanged("DescriptionFileName");
               
        //    }
        //}

        /// <summary>
        /// Contains the title of the lesson.
        /// </summary>
		public String Title {
			get {
				return _Description.Content;
			}
			set {
				_Description.Content = value;
				OnChanged("Description");
			}
		}
        public ContentNode _Description = new ContentNode();
       
        /// <summary>
        /// Contains the skills required content for this lesson.
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public String SkillsRequired
        {
            get
            {
                return _SkillsRequired.Content;
            }
            set
            {
                _SkillsRequired.Content = value;
                OnChanged("SkillsRequired");
            }
        }
        private ContentNode _SkillsRequired;
	

        [Bindable(BindableSupport.No)]
        public Boolean UseWavAudio { get { return SpeechPolicy.Equals(TTS.Configuration.Policies.Letters); } }
        
		/// <summary>
        /// Gets and Sets the Lesson Number.
        /// </summary>
        [Bindable(BindableSupport.Yes)]
        public int Number { 
            get
            { return _Number; } 
            set 
            {
                lessonStats.LessonNumber = value; 
                _Number = value;
                OnChanged("Number");
            }
        }
        private int _Number;

        /// <summary>
        /// Gets the Lesson Data. 
        /// Furthermore, Each element in the list represents a block of the lesson. 
        /// Each block is displayed indepedent of each other.
        /// </summary>
        private String LessonData { get; set; }
        private LessonStatistics lessonStats;

        [Bindable(BindableSupport.Yes)]
        public String Content {
            get
            {
                return _Content;
            }
            set
            {
                if (LessonData == null)
                {
                    LessonData = value.Trim();
                }
                 _Content = value;               
                OnChanged("Content");
                
            }
        }
        public String _Content;

        /// <summary>
        /// Specifies number of characters in a single lesson segment
        /// </summary>
        private const int CHARACTERS_IN_SEGMENT  = 4;

        /// <summary>
        /// Specifies the number of sentences in a single lesson segment.
        /// </summary>
        private const int SENTENCE_IN_SEGMENT    = 1;

        /// <summary>
        /// Specifies the number of words in a single lesson segment.
        /// </summary>
        private int WORDS_IN_SEGMENT             = 1;


       #endregion

       #endregion
       
       #region Static Methods

        /// <summary>
        /// Retrieves all the lesson types.
        /// </summary>
        /// <returns></returns>
        public static List<SignalCenter.TTS.Configuration.Policies> GetLessonTypes()
        {
            List<SignalCenter.TTS.Configuration.Policies> allLessonTypes = new List<SignalCenter.TTS.Configuration.Policies>();
            allLessonTypes.Add(SignalCenter.TTS.Configuration.Policies.Letters);
            allLessonTypes.Add(SignalCenter.TTS.Configuration.Policies.Words);
            allLessonTypes.Add(SignalCenter.TTS.Configuration.Policies.Sentences);


            return allLessonTypes;
        }
        #endregion 

       #region Public Methods
        /// <summary>
        /// Builds the lesson object into a string formated for a file.
        /// </summary>
        /// <returns></returns>
        public String BuildLesson()
        {
            String delimiter = ConfigurationManager.AppSettings["SectionDelimiter"].ToString();
            StringBuilder contents = new StringBuilder();
            contents.Append(delimiter);
            contents.Append("Speech Policy");
            contents.AppendLine(delimiter);
            contents.AppendLine((String.IsNullOrEmpty(SpeechPolicy.ToString())) ? "1" : Convert.ToInt32(SpeechPolicy) + "\n");

            contents.Append(delimiter);
            contents.Append("Description");
            contents.Append(delimiter);
            contents.AppendLine(Title);

            contents.Append(delimiter);
            contents.Append("Skills Required");
            contents.AppendLine(delimiter);
            contents.AppendLine((String.IsNullOrEmpty(_SkillsRequired.Content)) ? "No Skills Required" : _SkillsRequired.Content + "\n");

            contents.Append(delimiter);
            contents.Append("Skills Mastered");
            contents.AppendLine(delimiter);
            contents.AppendLine((String.IsNullOrEmpty(_MasterySkills.Content)) ? "No Skills Mastered after Completion" : _MasterySkills.Content + "\n");

            contents.Append(delimiter);
            contents.Append("Lesson Instructions");
            contents.AppendLine(delimiter);
            contents.AppendLine((String.IsNullOrEmpty(Instructions)) ? "No Lesson Instructions" : Instructions + "\n");

            contents.Append(delimiter);
            contents.Append("Lesson Data");
            contents.AppendLine(delimiter);
            contents.AppendLine(Content);

            return contents.ToString();
        }
        
        /// <summary>
        /// Overrides to string and places the lesson : number - title.
        /// </summary>
        /// <returns></returns>
        public override string ToString ()
		{
            return "Lesson " + _Number + ": " + _Description.Content;
			
		}
		
	
        /// <summary>
		/// Validate the key pressed against the first character in the content property.
		/// 
		/// </summary>
		/// <param name="e">The Key Pressed.</param>
        public virtual Boolean Validate(System.Windows.Forms.KeyEventArgs e, IKeyValidator validator)
        {
            String filePath;
            Char content  = 'a';
            content = Content[0];

            if (SignalCenter.Common.KeyValidator.Validate(e, content, out filePath))
            {
                Content = Content.Remove(0, 1);  
                validator.OnValidKeyDown(filePath);
                ProcessValidKeyPress();
                lessonStats.AddKey(true);
                return true;
            }
            else
            {
                lessonStats.AddKey(false);
                filePath = SignalCenter.Common.KeyTranslator.Instance.GetFilePath(content);
                validator.OnInvalidKeyDown(filePath);
                return false;
            }
        }

      


        public virtual void Randomize()
        {
            grammarList =  grammarList.Randomize<String>(randomizeAfterIndex);
        }




        /// <summary>
        /// Reset the position marker for where currently, in the lesson.
        /// Also, invokes the INotifyPropertyChanged Event for the Content property.
        /// </summary>
        public virtual void Reset()
        {
            if (_SpeechPolicy.Equals(TTS.Configuration.Policies.Sentences))
                elementsInSet = SENTENCE_IN_SEGMENT;
            else if (_SpeechPolicy.Equals(TTS.Configuration.Policies.Words))
                elementsInSet = WORDS_IN_SEGMENT;
            else
                elementsInSet = CHARACTERS_IN_SEGMENT;


            lastCompletedIndex = 0;
            currentCharacterIndex = 0;
            currentGrammarItemIndex = 0;
            endOfGrammarSegmentIndex = elementsInSet;
            Content = LessonData;

            grammarList = LessonData.InitializeGrammarList(SpeechPolicy);
            this.Randomize();

            //Ensures no IndexOutOfBounds Exception.
            if (endOfGrammarSegmentIndex >= grammarList.Count)
            {
                endOfGrammarSegmentIndex = grammarList.Count;
            }
            lessonStats = new LessonStatistics(this.Number);
            Content = GetRemainingSegment();
            OnGrammarSegmentChanged(Content);
        }

		/// <summary>
		/// Allows for exposing the remaining content in the current grammar segment of the lesson.
		/// </summary>
        /// <returns> A String representation of the remaining characters in the current grammar segment.</returns>
		public  String GetRemainingSegment()
		{
            if (SpeechPolicy == TTS.Configuration.Policies.Sentences)
            {
                return grammarList.GetSegment(0, currentGrammarItemIndex, endOfGrammarSegmentIndex);
            }
            else
            {
                return grammarList.GetSegment(lastCompletedIndex, currentGrammarItemIndex, endOfGrammarSegmentIndex);
            }
		} 
    
        #endregion

       #region Protected & Private Methods

        protected virtual void OnGrammerSegmentCompleted(String previousItem, TTS.Configuration.Policies type)
        {
            lastCompletedIndex = currentCharacterIndex;
            if (GrammarItemCompleted != null)
            {
                GrammarItemCompleted.Invoke(this, new GrammarItemCompletedEventArgs(previousItem, type));
            }
        }
      
        /// <summary>
        /// Fires the grammar segment changed event.
        /// </summary>
        /// <param name="newSegment"></param>
        protected virtual void OnGrammarSegmentChanged(String newSegment)
        {
            if (SegmentChanged != null)
                SegmentChanged.Invoke(this, new LessonSegmentEventArgs(newSegment));
        }  

        /// <summary>
        /// Fires the lesson completed event.
        /// </summary>
        protected virtual void OnCompleted()
        {
           // Reset();
            lessonStats.CalculateStatistics();
            LessonStatistics copy = lessonStats;
            
            if (LessonCompleted != null)
                LessonCompleted.Invoke(this, new LessonCompletedEventArgs(copy, true));


            lessonStats = new LessonStatistics(this.Number);
        }
      
        /// <summary>
        /// Fires the INotifyPropertyChanged event.
        /// </summary>
        /// <param name="caller"></param>
        private void OnChanged(string caller = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(caller));
            }
        }

        /// <summary>
        /// Responsible for tracking progress in the lesson. Additionally, tracks where in the grammar segment the lesson content field is,
        /// Lastly, is responsible for firing off the following events. GrammarSegmentCompleted, GrammarSegementChanged, and LessonCompleted.
        /// </summary>
        private void ProcessValidKeyPress()
        {
            //If the grammer list has not been initialized then reset mark
            if (grammarList == null)
            {
                Reset();
            }

            //Reached the end of the current character, word, or sentence.
    if (currentCharacterIndex == grammarList[currentGrammarItemIndex].Length - 1)                     
// if ((SpeechPolicy != TTS.Configuration.Policies.Words && currentCharacterIndex == grammarList[currentGrammarItemIndex].Length - 1) || (SpeechPolicy == TTS.Configuration.Policies.Words && grammarList[currentGrammarItemIndex][currentCharacterIndex].Equals(' ')))
            {
                //Gets the Completed Word, Sentence, or character.
                String segmentCompleted = grammarList.GetSegment(0, currentGrammarItemIndex, currentGrammarItemIndex + 1);
                OnGrammerSegmentCompleted(segmentCompleted, SpeechPolicy);


                //Resets character index to the beginning of the next word. Additionally, moves to the next item in the list.
                currentCharacterIndex = 0;
                currentGrammarItemIndex++;
                if (currentGrammarItemIndex == endOfGrammarSegmentIndex)
                {
                    //Condition for end of lesson detection 
                    if (endOfGrammarSegmentIndex >= grammarList.Count - 1)
                    {
                        OnCompleted();
                        return;
                    }

                    endOfGrammarSegmentIndex += elementsInSet;
                    //Preventing ArrrayOutOfBounds Exception
                    if (endOfGrammarSegmentIndex >= grammarList.Count)
                    {
                        endOfGrammarSegmentIndex = grammarList.Count;
                    }

                    String newSegment = GetRemainingSegment();
                    OnGrammarSegmentChanged(newSegment);
                    Content = newSegment;
                }
            }
            else
            { 
                //Checks for end of a word in a sentence lesson.
               // if (SpeechPolicy.Equals(TTS.Configuration.Policies.Sentences))
               if(!SpeechPolicy.Equals(TTS.Configuration.Policies.Letters))
                    {
                    String currentSentence = grammarList[currentGrammarItemIndex];
                    if (currentSentence[currentCharacterIndex + 1].Equals(' '))
                    {
                        int endOfWord = currentCharacterIndex;
                        int startOfWord = currentSentence.LastIndexOf(' ', currentCharacterIndex - 1);
                        startOfWord = (startOfWord == -1) ? 0 : startOfWord;   
                        String previousWord = currentSentence.Substring(startOfWord, endOfWord - startOfWord);
                        OnGrammerSegmentCompleted(previousWord, TTS.Configuration.Policies.Words);
                    
                    }

                }
                currentCharacterIndex++;
            }
        }

        #endregion
    
    }

}