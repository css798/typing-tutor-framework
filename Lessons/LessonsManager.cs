﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using SignalCenter.Lessons;
using System.Configuration;
using SignalCenter.Lessons.Enums;
using SignalCenter.Common.Util;

namespace SignalCenter.Lessons
{
    /// <summary>
    /// Provides file management of lessons for the Typing Tutor.
    /// All file I/O mapping is based upon three components.
    ///     First, the directory in which to get to the lessons folders. This is defined in the App.Config file under the LessonDirectory key.
    ///     Secondly, the lesson number will specify the folder to look into. This is typically specified as a parameter or inside of a Lesson object that is passed as a parameter.
    ///     Lastly, the base file name and extension that has to be constant for all lesson files. This can be defined in the App.Config in two seperate keys first the baseFileName and additionally, the FileExtension key specifies the extension for all lessons files.
    /// 
    /// The LessonManager also uses two delimiters for parsing purposes. Both of which are defined in the App.Config.
    ///       First, is the section sectionDelimiter. This specifies the beginning and end of a section in a lesson file. This is is called SectionDelimiter.
    ///       Secondly, is the non printable character sectionDelimiter. This specifies the beginning and end of strings that are not mapped to characters. This is internally used to map non printable keys to a single character that is defined in the unicode map. 
    ///       The key for this sectionDelimiter is the NonPrintableCharacterDelimiter.
    /// 
    /// </summary>
    public class LessonsManager
    {

        #region Constructors
        static LessonsManager()
        {
            Instance = new LessonsManager();
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        private LessonsManager()
        {
            LessonCount = new List<int>();
            RefreshLessons(); 
        }

        #endregion


        #region Properties and Members

        /// <summary>
        /// Gets the total available lessons. This dictionary maps the lesson number to the file path.
        /// </summary>
        public  static ICollection<int> LessonCount { get; private set; } //THIS MUST REMAIN STATIC

        /// <summary>
        /// List of all lessons that are available.
        /// </summary>
        public List<Lesson> Lessons { get; private set; }

        /// <summary>
        /// Gets and sets the current lesson being used in the application. Upon being set Lesson.Reset() is invoked on the new current lesson.
        /// </summary>
        public Lesson CurrentLesson 
        {
            get { return _CurentLesson; }
            set{ 
                     _CurentLesson = value;
                   
                }
        }
        private Lesson _CurentLesson;

       // public static LessonsManager _Instance;
        public static LessonsManager Instance
        {
            get;
            private set;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Refreshes the Manager.  Clears all manager properties and reassigns them by going to the drive and parsing the files.
        /// If an exception occured while reading any lesson, then TTS plays the string specified in Constants.LESSON_PARSE_FAILURE.
        /// </summary>
        /// <returns></returns>
        public Boolean RefreshLessons()
        {
            Boolean results = true;
            RefreshAvailableLessons();
            List<Lesson> temp;
            results = LessonsManager.ReadAll(out temp);
            Lessons = temp;

            if (results == false)
            {
                TTS.Engine.Play(SignalCenter.Common.Constants.LESSON_PARSE_FAILURE, TTS.Configuration.Policies.Sentences, TTS.Configuration.Voices.Male);
            }
            return results;
        }

        /// <summary>
        /// Refreshes the LessonCount property of the manager.
        /// </summary>
        public void RefreshAvailableLessons()
        {
            LessonCount = FindNumberOfLessonsAvailable();
        }

        #endregion


        #region Static Methods

        #region Public Methods
        /// <summary>
        /// Creates and Stores a new lesson. 
        /// Furthermore, creates a virtual File that represents the lessons.
        /// Does not allow creating files that already exists. If this is desired use
        /// replace method. 
        /// Additionally, this Number property of the lesson MUST be greater then the TotalLessons. Otherwise, returns false
        /// </summary>
        /// <param name="newLesson">Lesson to be created</param>
        /// <returns>True, if the file has been created successfully.</returns>
        public static Boolean Create(Lesson newLesson)
        {
            FindNumberOfLessonsAvailable();

            if (LessonsManager.Exists(newLesson.Number))
                return false;
            if (String.IsNullOrEmpty(newLesson.Content))
                return false;
            if (LessonCount.Contains(newLesson.Number))
                return false;


            string filename = IOBuilder.BuildFilePath(newLesson.Number);
            StreamWriter lessonWriter = null;

            try
            {
                lessonWriter = new StreamWriter(filename, true, Encoding.Unicode);
                String contents = newLesson.BuildLesson();
                contents = TransformCharacters(contents, true);
                lessonWriter.Write(contents.ToString());
                LessonCount.Add(newLesson.Number);
                return true;

            }
            catch (DirectoryNotFoundException lessonDirectoryNotFound)
            {
                try
                {
                    String path = IOBuilder.BuildDirectoryPath(newLesson.Number);
                    Directory.CreateDirectory(path);
                }
                catch (Exception e)
                {
                    return false;
                }
                return Create(newLesson);
            }
            catch (IOException e)
            {
                return false;
            }
            finally
            {
                if (lessonWriter != null)
                    lessonWriter.Close();
            }
        }

        /// <summary>
        /// Reads the lesson. Furthermore, opens the file corresponding to the lessonNumber and creates a lesson object to be returned.
        /// </summary>
        /// <param name="lessonNumber">The lesson Number to be read</param>
        /// <param name="lessonRead">The lesson to populate </param>
        /// <exception cref="FormatException"> Thrown after reading data and the lesson does not meet minimum section requirements</exception>
        /// <returns>True if the lesson was successfully read.</returns>
        public static Boolean Read(int lessonNumber, out Lesson lessonRead)
        {
           
            lessonRead = null;
            DateTime startTime = DateTime.Now;
            if (!LessonCount.Contains(lessonNumber))
            {
                lessonRead = null;
                return false;
            }

            //Used for Handling I/O Operations
            string filePath = IOBuilder.BuildFilePath(lessonNumber);
            StreamReader lessonReader = null;

            //Used for Storage Containers in Sections
            Char[] buffer = new Char[1];
            StringBuilder sectionContent = new StringBuilder();
            Sections currentSection = Sections.None;
            String sectionDelimiter = ConfigurationManager.AppSettings["SectionDelimiter"].ToString();
            Boolean hasContentPropSet = false;
            try
            {
                lessonRead = new Lesson();
                lessonRead.Number = lessonNumber;
                lessonReader = new StreamReader(filePath, Encoding.Default);
                ReadUntilDelimiter(ref lessonReader, sectionDelimiter[0]);
                Boolean ignoreCharacters = true;
                while (lessonReader.Read(buffer, 0, buffer.Length) != 0 || hasContentPropSet)
                {
                    //Handles State Changes from Ignoring Characters and Storing Characters
                    if (!sectionDelimiter.Contains(buffer[0]) && !lessonReader.EndOfStream)
                        ignoreCharacters = false;
                    else if (sectionDelimiter.Contains(buffer[0]))
                    {
                        ignoreCharacters = true;
                        
                    }
                    else if (lessonReader.EndOfStream)
                    {
                        ignoreCharacters = true;
                        sectionContent.Append(buffer[0]);
                    }

                    //Handles the Case when the reading is not within a Section Header.
                    if (ignoreCharacters == false)
                        sectionContent.Append(buffer[0]);
                    else
                    {
                        //Handles case when the StreamReader has not reached the end of the first section yet.
                        if (currentSection != 0)
                        {
                            //This will be utilized later.
                            String content = LessonsManager.TransformCharacters(sectionContent.ToString(), true);

                            //Handles mapping the section number to the lesson property.
                            AddContentToLesson(ref lessonRead, content, currentSection);

                            //Reseting the Content of the nextSection
                            sectionContent = new StringBuilder();
                        }

                        //Reads until the end of the Next Character will be the Section Header.
                        ReadUntilDelimiter(ref lessonReader, sectionDelimiter[0]);
                        lessonReader.Read();
                        ignoreCharacters = false;
                        currentSection++;
                    }
                }
                Console.WriteLine("DEBUG: Reading Alg. Took " + (DateTime.Now - startTime).Duration().ToString());
                return true;
            }
            catch (IOException e)
            {
                lessonRead = null;
                return false;
            }
            finally
            {
                if (lessonReader != null)
                    lessonReader.Close();
            }
        }

        /// <summary>
        /// Reads all lessons. Furthermore, reads all files in the lessons directory. Constructs a list of all lessons.
        /// 
        /// </summary>
        /// <param name="allLessons"> initializes a new list, and adds new lessons read to the list.</param>
        /// <returns>The success of the operation. If false, their was an issue reading one or more of the lessons.</returns>
        public static Boolean ReadAll(out List<Lesson> allLessons)
        {
           Boolean results =  true;
           SortedSet<Lesson> temp = new SortedSet<Lesson>(new SignalCenter.Lessons.LessonComparer());
           foreach(int lessonNumber in LessonCount)
            {
                Lesson aLesson = null;
                if (Read(lessonNumber, out aLesson))
                    temp.Add(aLesson);
                else
                    results = false;
            }

            allLessons = temp.ToList<Lesson>();
            return results;
        }

        /// <summary>
        /// Replaces an existing lesson.
        /// Furthermore, Checks to see if the file exists.
        /// Then deletes the file and will create the new file to represent the lesson.
        /// </summary>
        /// <param name="lesson">Lesson to be replaced. The Number property is associated with the lesson to be replaced with</param>
        ///  <returns>True
        ///                 if the replacement was successful.
        ///           False
        ///                 if the file does not exists, deletion was unsuccessful, or creation of the file.
        /// </returns>
        public static Boolean Replace(Lesson lesson)
        {

            if (!LessonsManager.Exists(lesson.Number))
                return false;

            Lesson oldLesson;
            LessonsManager.Read(lesson.Number, out oldLesson);
            Boolean results = false;
            try
            {
                String filePath = IOBuilder.BuildFilePath(lesson.Number);
                File.Delete(filePath);
                LessonCount.Remove(lesson.Number);
                results = LessonsManager.Create(lesson);
                if (!results)
                {
                    LessonsManager.Create(oldLesson);
                }

            }
            catch (IOException e)
            {
                LessonsManager.Create(oldLesson);
               
            }

            return results;
        }

        /// <summary>
        /// Checks to see if the lesson exists. 
        /// Furthermore, the criteria for checking is based on the file path.
        /// </summary>
        /// <param name="lessonNumber">The lesson number to check.</param>
        /// <returns>True, if the lesson exists.</returns>
        public static Boolean Exists(int lessonNumber)
        {
            Boolean results = false;
            try
            {
                String filePath = IOBuilder.BuildFilePath(lessonNumber);
                if (File.Exists(filePath))
                    results = true;
            }
            catch (IOException e)
            { results = false; }


            return results;
        }


        /// <summary>
        /// Transforms the Physical representation of characters into a string of unicode characters.
        /// </summary>
        /// <param name="rawContent">Untouched Content from the file reading. This is an entire section.</param>
        /// <param name="direction">Indicates the direction of which the rawContent is headed. 
        ///                             If true, the content is being loaded into a container (I.E. an List)
        ///                             If false, the content is being written to a file.</param>
        public static String TransformCharacters(String rawContent, Boolean direction)
        {
            Char[] rawCharacters = rawContent.ToCharArray();
            String sanitizedData = rawContent;
            Char nonPrintedCharactersDelimiter = ConfigurationManager.AppSettings["NonPrintableCharacterDelimiter"].ToString().ToCharArray()[0];
            StringBuilder nonPrintedCharacters = new StringBuilder();
            if (!direction)
            {
                foreach (Char character in rawCharacters)
                {
                    //This Represents the Value that a Non Printable Character is Mapped to. 
                    //By the standards set forth, it will be encapsulated with `
                    String printedValue;
                    if (SignalCenter.Common.KeyTranslator.Instance.GetNonPrintedCharacterToFile(character, out printedValue))
                    {
                        sanitizedData = sanitizedData.Replace(character.ToString(), printedValue);
                    }

                }
            }
            else
            {
                for (int i = 0; i < rawCharacters.Length; i++)
                {
                    if (rawCharacters[i].Equals(nonPrintedCharactersDelimiter))
                    {
                        do
                        { nonPrintedCharacters.Append(rawCharacters[i++]); }
                        while (!rawCharacters[i].Equals(nonPrintedCharactersDelimiter) || i == rawCharacters.Length);

                        if (i != rawCharacters.Length)
                        {
                            nonPrintedCharacters.Append(nonPrintedCharactersDelimiter);

                            String nonPrintedValue;
                            if (!SignalCenter.Common.KeyTranslator.Instance.GetNonPrintedCharacter(nonPrintedCharacters.ToString(), out nonPrintedValue))
                                throw new Exception("No Mapping Detected");

                           // Console.Write("DEBUG: Raw Input Before Replacement: " + sanitizedData);
                            sanitizedData = sanitizedData.Replace(nonPrintedCharacters.ToString(), nonPrintedValue);
                          //  Console.WriteLine("DEBUG: Sanitized Input: " + sanitizedData);
                            nonPrintedCharacters = new StringBuilder();
                        }
                    }
                }
            }
            return sanitizedData;
        }

        #endregion


        #region Private Methods


        /// <summary>
        /// Adds content to a lesson object based on the section number of the reading.
        /// IF the section is the SpeechPolicy then <code> lesson = Lesson.createLessonObj(Policy)</code>
        /// </summary>
        /// <param name="lesson">The lesson to add the section data to.</param>
        /// <param name="content">The data parsed from the reader.</param>
        /// <param name="CurrentSection">Section that the content is associated with</param>
        private static void AddContentToLesson(ref Lesson lesson, String content, Sections CurrentSection)
        {
            switch (CurrentSection)
            {
                case Sections.SpeechPolicy:
                    int speechpolicy;
                    if (Int32.TryParse(content.Trim(), out speechpolicy))
                    {
                        SignalCenter.TTS.Configuration.Policies temp;
                        Enum.TryParse<SignalCenter.TTS.Configuration.Policies>(content.Trim(), out temp);
                        lesson.SpeechPolicy = temp;
                    }

                    break;
                case Sections.Skills_Required:
                    lesson.SkillsRequired  = content.Trim();
                    break;
                case Sections.Description:
                    lesson.Title = content.Trim();
                    break;
                case Sections.Skills_Mastered:
                    lesson.MasterySkills =  content.Trim();
                    break;
                case Sections.Instructions:
                    lesson.Instructions = content.Trim();
                    break;
                case Sections.LessonContent:
                    StringBuilder lessonData = new StringBuilder();
                    foreach (String line in content.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        lessonData.Append(line);
                        lessonData.Append(" ");
                    }

                    lesson.Content = lessonData.ToString();
                    break;
            }
        }

        /// <summary>
        /// Reads until the next Delimiter.
        /// </summary>
        /// <param name="lessonReader"></param>
        private static void ReadUntilDelimiter(ref StreamReader lessonReader, Char delimiter)
        {
            while (!lessonReader.Peek().Equals((int)delimiter) && !lessonReader.EndOfStream)
            {
                lessonReader.Read();
                //Console.Write(buffer[0]);
            }
        }


        /// <summary>
        /// Gets the total lessons that exists in the specified directories.
        ///<returns> A dictionary that maps the lesson number to the full file path.</returns>
        /// </summary>
        private static ICollection<int> FindNumberOfLessonsAvailable()
        {
            ICollection<int> temp = new List<int>();
            String lessonDirectoryPath = ConfigurationManager.AppSettings["LessonDirectory"].ToString();
            try
            {
                String currentDirectory = SignalCenter.Common.Util.IOBuilder.BuildProgramDataDirectory();
                //Directory that houses all individual lesson directories.
                String[] lessonDirectories = Directory.GetDirectories(System.IO.Path.Combine(currentDirectory, lessonDirectoryPath));
                String fileName = IOBuilder.BuildFileName();
                foreach(String aLessonDir in lessonDirectories)
                {
                    int lessonNumber = -1;
					String number = aLessonDir.Split(Path.DirectorySeparatorChar).LastOrDefault();
                   // String number = aLessonDir.Substring(aLessonDir.LastIndexOf('\\') + 1);
                    if (Int32.TryParse(number, out lessonNumber))
                    {
                        String path = IOBuilder.BuildDirectoryPath(lessonNumber);
                        IEnumerable<String> files = Directory.GetFiles(path).Where<String>(a => a.EndsWith(fileName));
                        if (files.Count() > 0)
                        {
                            temp.Add(lessonNumber);
                        }
                    }
                }


            }
            catch (DirectoryNotFoundException ex)
            {
                System.Windows.Forms.MessageBox.Show("We are sorry, the application has encountered an error.Please contact an Adminstrator for assistance."+
                    "Additional Notes: Unabled to locate directory for lessons. This application will become unstable due to unable to find the directory of the lessons.");
            }

            return temp;
        }


        #endregion



        #endregion
    }
}