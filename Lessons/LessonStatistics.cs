
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Configuration;
using SignalCenter.Common.Util;

namespace SignalCenter.Lessons
{
	
	/// <summary>
	/// Allows for progress tracking for a given lesson.
	/// </summary>
	[Serializable]
	public class LessonStatistics
	{
			#region Properties

			/// <summary>
			/// Gets the character accuracy in percentage form.
			/// </summary>			
			public Decimal Accuracy { get; private set; }			
			
			/// <summary>
			/// Gets and Sets the Number Associated with the Statistics
			/// </summary>			
			public int LessonNumber { get; set; }
			
			/// <summary>
			/// Gets the Time Span between the Start of the First Clocked Character
			/// and the call to StopTimer method.
			/// </summary>
			public TimeSpan ElapsedTime { get; private set;}
			private DateTime startTime;
			private DateTime endTime;
			
			/// <summary>
			/// Gets the Keys Per Minute statistic
			/// </summary>
			public Decimal KeysPerMinute  { get; private set;}
			public Int32 totalKeys;
			
			/// <summary>
			/// Gets the WordsPerMinute
			/// </summary>
			public Decimal WordsPerMinute { get; private set; }
			private Int32 totalWords;
			
			/// <summary>
			/// Gets the character error count.
			/// </summary>
			public Int32 Errors { get ;  private set; }
			
			#endregion 		
			
			#region Constructors

            /// <summary>
            /// Default Constructor
            /// </summary>
            /// <param name="lessonNumber"></param>
			public LessonStatistics(int lessonNumber)
			: this(lessonNumber, 0 ,  0, 0, 0, new TimeSpan())
            {
            
            }

            public LessonStatistics(int lessonNumber,int errors, Decimal wpm, Decimal cpm, Decimal accuracy, TimeSpan elapsedTime)
            {
                LessonNumber = lessonNumber;
                Errors = errors;
                WordsPerMinute = wpm;
                KeysPerMinute = cpm;
                Accuracy = accuracy;
                ElapsedTime = elapsedTime;

            }
			
			
			#endregion 
			
			#region Public Methods
			
			/// <summary>
			/// Calculates all Stats
			/// </summary>
			/// <returns></returns>
			public LessonStatistics CalculateStatistics()
			{
				this.StopTimer();
				
				Decimal temp = new Decimal((totalKeys - Errors * 1.0f) / totalKeys * 100);
				Accuracy = temp;
				
				//Lesson Duration in Minutes
				Double duration = ElapsedTime.TotalMinutes;
				KeysPerMinute =  new Decimal(totalKeys / duration);
				WordsPerMinute = new Decimal(totalWords / duration);
				
				return this;
			}
			
			/// <summary>
			/// Keeps track of all Statistics.
			/// Adds a character to the total amount of characters.
			/// Furthermore, if the timer has not started for the duration. Then it begins the elapsed time.
			/// The algorithm for determining a word is 5 key presses, which is defined by any punctutation, space, or character.
			/// Furthermore, the algorithm takes into consideration the character errors as well.
			/// Therefore the formula is defined as  (Total Keys Pressed - Character Errors) % 5 == 0.
			/// </summary>
			/// <param name="isSuccess">If false, updates the error count.</param>
			public void AddKey(Boolean isSuccess)
			{
				
				if (startTime.Year < 2011)
					startTime = DateTime.Now;
				
				totalKeys++;
				
				if (!isSuccess)
					Errors++;
				else
					correctCharacters++;
				// Console.Write("DEBUG: CorrectChars % 5 =" + correctCharacters % 5);
				
				if((correctCharacters % 5) == 0)
					totalWords++;
			}
			private int correctCharacters = 0;
			
			
			
#endregion
			
			#region Private Methods
            public LessonStatistics StartTimer()
            {

                startTime = DateTime.Now;
                ElapsedTime = new TimeSpan();
                return this;
            }

			/// <summary>
			/// Stops the Timer
			/// </summary>
			public LessonStatistics StopTimer()
			{
				endTime = DateTime.Now;
				ElapsedTime = endTime.Subtract(startTime);

				return this;
			}
			
#endregion

            #region Static Methods
            /// <summary>
            /// Rounds all Properties in LessonStatistics
            /// </summary>
            /// <param name="statistics"></param>
            public static LessonStatistics RoundAll(LessonStatistics statistics)
            {
                LessonStatistics roundedStats = new LessonStatistics(statistics.LessonNumber);
                roundedStats.KeysPerMinute = Math.Round(statistics.KeysPerMinute, 0);
                roundedStats.WordsPerMinute = Math.Round(statistics.WordsPerMinute, 0);
                roundedStats.Accuracy = Math.Round(statistics.Accuracy, 0);
                roundedStats.Errors = statistics.Errors;
                roundedStats.ElapsedTime = statistics.ElapsedTime;

                return roundedStats;
            }
            #endregion

    }
}

