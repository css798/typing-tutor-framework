﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignalCenter.Common
{
    /// <summary>
    /// Contains all constants used in the SignalCenter common library.
    /// </summary>
    public static class Constants
    {

        public static readonly Boolean PLAY_VALID_KEY_PRESS = false;

        public static readonly Boolean IGNORE_VALID_KEY_PRESS = true;

        public static readonly bool IsWindows = Type.GetType("Mono.Runtime") == null;

        public static String APPLICATION_LOAD_MESSAGE = " Welcome to the accessible typing tutor software. This program will help you become familar with keys on a keyboard. ";

        public static String LESSON_LOAD_FAILURE = "We are sorry for this inconvience, the lesson you have selected was unable to be loaded";

        public static String LESSON_PARSE_FAILURE = "We are unable to completely read all the lessons from the disk.";


        /// <summary>
        /// Constant represents the wording to be used for  a general synchronization error.
        /// </summary>
        public static readonly String SYNCHRONIZATION_FAILURE = "A general failure occured while a synchronization attempt was in progress.";


        public static String WAV_FILE_NOT_FOUND = "We are unable to find the specified sound file. Please check with your administrator.";


#if WINDOWS
        public static readonly string XML_PATH = System.IO.Path.Combine(SignalCenter.Common.Util.IOBuilder.BuildProgramDataDirectory(), "UserList.XML");
#elif MAC_OSX

		public static readonly string XML_PATH = System.IO.Path.Combine(SignalCenter.Common.Util.IOBuilder.BuildProgramDataDirectory(),"UserList.XML");
#endif
		public static readonly string DATALOGGING_XML_PATH = System.IO.Path.Combine(SignalCenter.Common.Util.IOBuilder.BuildProgramDataDirectory(),"UserData.xml");
    }
}