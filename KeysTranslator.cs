﻿
//                   Copyright 2013.
//    James Adam Armstrong, Aaron Messer, Corey Mann,
//         Jackson Dean Goodwin, Amber Harrison
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace SignalCenter.Common
{
    public sealed class KeyTranslator
    {
        private static KeyTranslator instance = null;
        private static Dictionary<string, string> keyCodeDictionary;
        private static Dictionary<string, string> shiftKeyCodeDictionary;
        private static Dictionary<string, string> charactersDictionary;
        private static Dictionary<String, String> nonPrintableCharacters;

        private KeyTranslator()
        {
            InitializeKeyCodeDictionary();
            InitializeShiftKeyCodeDictionary();
            InitializeCharactersDictionary();
            InitializeNonPrintableCharactersDictionary();
        }

        public static KeyTranslator Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new KeyTranslator();
                }
                return instance;
            }
        }


       // private Boolean isCapsOn = false;
        public string GetFilePath(KeyEventArgs keyPressed)
        {
            string originalKeyCode = keyPressed.KeyCode.ToString();
            string translatedKeyCode = String.Empty;

            if (!keyCodeDictionary.TryGetValue(originalKeyCode, out translatedKeyCode))
            {
                Console.WriteLine("KeyCode not mapped in keyCodeDictionary");
                return "UnrecognizedKeyPressed.wav";
            }

            Boolean isLetter = translatedKeyCode.Length == 1;
            
            //Base Case is if the key pressed a Letter.
            if(isLetter)
            {
                if(Control.IsKeyLocked(Keys.CapsLock))
                {
                     if(!keyPressed.Shift)
                     {
                         shiftKeyCodeDictionary.TryGetValue(translatedKeyCode, out translatedKeyCode);
                     }
                }
                else if(!Control.IsKeyLocked(Keys.CapsLock))
                {

                    if(keyPressed.Shift)
                    {
                        shiftKeyCodeDictionary.TryGetValue(translatedKeyCode, out translatedKeyCode);
                    }
                }
            }
            else if (!isLetter)
            {
                if (keyPressed.Shift)
                {
                    shiftKeyCodeDictionary.TryGetValue(translatedKeyCode, out translatedKeyCode);
                }
            }
            return translatedKeyCode + ".wav";

        }

        public string GetFilePath(char currentChar)
        {
            string originalCharacter = currentChar.ToString();
            string translatedCharacter = String.Empty;
            charactersDictionary.TryGetValue(originalCharacter, out translatedCharacter);            
            string filePath =  translatedCharacter + ".wav";
            return filePath;
        }

        public List<string> GetFilePath(string text)
        {
            List<string> filePaths = new List<string>();
            string aFilePath = String.Empty;
            foreach (char c in text.ToCharArray())
            {
                aFilePath = GetFilePath(c);
                filePaths.Add(aFilePath);
            }
            return filePaths;
        }
        /// <summary>
        /// Gets the non printed value for a string of characters.
        /// </summary>
        /// <param name="rawInput"></param>
        /// <param name="nonPrintedValue">The value corresponding to the rawInput will be placed into this parameter</param>
        /// <returns></returns>
        public Boolean GetNonPrintedCharacter(String rawInput, out String nonPrintedValue)
        {
            String value = null;
            Boolean results = false;
            if (nonPrintableCharacters.TryGetValue(rawInput.ToUpper(), out value))
                    results = true;
            
            nonPrintedValue = value;
            return results;
        }
        /// <summary>
        /// This maps the non printable character to the string representation.
        /// </summary>
        /// <param name="rawInput"></param>
        /// <param name="printedValue"></param>
        /// <returns></returns>
        public Boolean GetNonPrintedCharacterToFile(Char character, out String printedValue)
        {
            String value = printedValue = null;
            Boolean results = false;
            Dictionary<String, String>.Enumerator iterator = nonPrintableCharacters.GetEnumerator();
            while (iterator.MoveNext())
            {
                KeyValuePair<String, String> pair = iterator.Current;
                if(pair.Value.Equals(character.ToString()))
                {
                    printedValue = pair.Key;
                    return true;
                }
            }
            return results;
        }
        private static void InitializeKeyCodeDictionary()
        {
            keyCodeDictionary = new Dictionary<string, string>();
            keyCodeDictionary.Add("A", "A");
            keyCodeDictionary.Add("Add", "Plus");
            keyCodeDictionary.Add("Apps", "Menu");
            keyCodeDictionary.Add("B", "B");
            keyCodeDictionary.Add("Back", "Backspace");
            keyCodeDictionary.Add("C", "C");
            keyCodeDictionary.Add("Capital", "CapsLock");
            keyCodeDictionary.Add("CapsLock", "CapsLock");
            keyCodeDictionary.Add("ControlKey", "Control");
            keyCodeDictionary.Add("D", "D");
            keyCodeDictionary.Add("D0", "Zero");
            keyCodeDictionary.Add("D1", "One");
            keyCodeDictionary.Add("D2", "Two");
            keyCodeDictionary.Add("D3", "Three");
            keyCodeDictionary.Add("D4", "Four");
            keyCodeDictionary.Add("D5", "Five");
            keyCodeDictionary.Add("D6", "Six");
            keyCodeDictionary.Add("D7", "Seven");
            keyCodeDictionary.Add("D8", "Eight");
            keyCodeDictionary.Add("D9", "Nine");
            keyCodeDictionary.Add("Decimal", "Period");
            keyCodeDictionary.Add("Delete", "Delete");
            keyCodeDictionary.Add("Divide", "ForwardSlash");
            keyCodeDictionary.Add("Down", "Down");
            keyCodeDictionary.Add("E", "E");
            keyCodeDictionary.Add("End", "End");
            keyCodeDictionary.Add("Enter", "Enter");
            keyCodeDictionary.Add("Escape", "Escape");
            keyCodeDictionary.Add("F", "F");
            keyCodeDictionary.Add("F1", "F1");
            keyCodeDictionary.Add("F10", "F10");
            keyCodeDictionary.Add("F11", "F11");
            keyCodeDictionary.Add("F12", "F12");
            keyCodeDictionary.Add("F2", "F2");
            keyCodeDictionary.Add("F3", "F3");
            keyCodeDictionary.Add("F4", "F4");
            keyCodeDictionary.Add("F5", "F5");
            keyCodeDictionary.Add("F6", "F6");
            keyCodeDictionary.Add("F7", "F7");
            keyCodeDictionary.Add("F8", "F8");
            keyCodeDictionary.Add("F9", "F9");
            keyCodeDictionary.Add("G", "G");
            keyCodeDictionary.Add("H", "H");
            keyCodeDictionary.Add("Home", "Home");
            keyCodeDictionary.Add("I", "I");
            keyCodeDictionary.Add("Insert", "Insert");
            keyCodeDictionary.Add("J", "J");
            keyCodeDictionary.Add("K", "K");
            keyCodeDictionary.Add("L", "L");
            keyCodeDictionary.Add("LControlKey", "Control");
            keyCodeDictionary.Add("Left", "Left");
            keyCodeDictionary.Add("LMenu", "Alt");
            keyCodeDictionary.Add("LShiftKey", "Shift");
            keyCodeDictionary.Add("LWin", "Windows");
            keyCodeDictionary.Add("M", "M");
            keyCodeDictionary.Add("Menu", "Alt");
            keyCodeDictionary.Add("Multiply", "Asterisk");
            keyCodeDictionary.Add("N", "N");
            keyCodeDictionary.Add("Next", "PageDown");
            keyCodeDictionary.Add("NumLock", "NumLock");
            keyCodeDictionary.Add("NumPad0", "Zero");
            keyCodeDictionary.Add("NumPad1", "One");
            keyCodeDictionary.Add("NumPad2", "Two");
            keyCodeDictionary.Add("NumPad3", "Three");
            keyCodeDictionary.Add("NumPad4", "Four");
            keyCodeDictionary.Add("NumPad5", "Five");
            keyCodeDictionary.Add("NumPad6", "Six");
            keyCodeDictionary.Add("NumPad7", "Seven");
            keyCodeDictionary.Add("NumPad8", "Eight");
            keyCodeDictionary.Add("NumPad9", "Nine");
            keyCodeDictionary.Add("O", "O");
            keyCodeDictionary.Add("Oem1", "Semicolon");
            keyCodeDictionary.Add("Oem102", "Backslash");
            keyCodeDictionary.Add("Oem2", "ForwardSlash");
            keyCodeDictionary.Add("Oem3", "Grave");
            keyCodeDictionary.Add("Oem4", "LeftBracket");
            keyCodeDictionary.Add("Oem5", "Backslash");
            keyCodeDictionary.Add("Oem6", "RightBracket");
            keyCodeDictionary.Add("Oem7", "Apostrophe");
            keyCodeDictionary.Add("OemBackslash", "Backslash");
            keyCodeDictionary.Add("OemCloseBrackets", "RightBracket");
            keyCodeDictionary.Add("Oemcomma", "Comma");
            keyCodeDictionary.Add("OemMinus", "Hyphen");
            keyCodeDictionary.Add("OemOpenBrackets", "LeftBracket");
            keyCodeDictionary.Add("OemPeriod", "Period");
            keyCodeDictionary.Add("OemPipe", "Backslash");
            keyCodeDictionary.Add("Oemplus", "Equals");
            keyCodeDictionary.Add("OemQuestion", "ForwardSlash");
            keyCodeDictionary.Add("OemQuotes", "Apostrophe");
            keyCodeDictionary.Add("OemSemicolon", "Semicolon");
            keyCodeDictionary.Add("Oemtilde", "Grave");
            keyCodeDictionary.Add("P", "P");
            keyCodeDictionary.Add("PageDown", "PageDown");
            keyCodeDictionary.Add("PageUp", "PageUp");
            keyCodeDictionary.Add("Pause", "Break");
            keyCodeDictionary.Add("PrintScreen", "PrintScreen");
            keyCodeDictionary.Add("Prior", "PageUp");
            keyCodeDictionary.Add("Q", "Q");
            keyCodeDictionary.Add("R", "R");
            keyCodeDictionary.Add("RControlKey", "Control");
            keyCodeDictionary.Add("Return", "Enter");
            keyCodeDictionary.Add("Right", "Right");
            keyCodeDictionary.Add("RMenu", "Alt");
            keyCodeDictionary.Add("RShiftKey", "Shift");
            keyCodeDictionary.Add("RWin", "Windows");
            keyCodeDictionary.Add("S", "S");
            keyCodeDictionary.Add("Scroll", "ScrollLock");
            keyCodeDictionary.Add("ShiftKey", "Shift");
            keyCodeDictionary.Add("Snapshot", "PrintScreen");
            keyCodeDictionary.Add("Space", "Space");
            keyCodeDictionary.Add("Subtract", "Hyphen");
            keyCodeDictionary.Add("T", "T");
            keyCodeDictionary.Add("Tab", "Tab");
            keyCodeDictionary.Add("U", "U");
            keyCodeDictionary.Add("Up", "Up");
            keyCodeDictionary.Add("V", "V");
            keyCodeDictionary.Add("W", "W");
            keyCodeDictionary.Add("X", "X");
            keyCodeDictionary.Add("Y", "Y");
            keyCodeDictionary.Add("Z", "Z");
        }

        private static void InitializeShiftKeyCodeDictionary()
        {
            shiftKeyCodeDictionary = new Dictionary<string, string>();
            shiftKeyCodeDictionary.Add("A", "CapA");
            shiftKeyCodeDictionary.Add("Apostrophe", "DoubleQuotes");
            shiftKeyCodeDictionary.Add("B", "CapB");
            shiftKeyCodeDictionary.Add("Backslash", "Bar");
            shiftKeyCodeDictionary.Add("C", "CapC");
            shiftKeyCodeDictionary.Add("Comma", "LessThan");
            shiftKeyCodeDictionary.Add("D", "CapD");
            shiftKeyCodeDictionary.Add("Down", "Down");
            shiftKeyCodeDictionary.Add("E", "CapE");
            shiftKeyCodeDictionary.Add("Eight", "Asterisk");
            shiftKeyCodeDictionary.Add("Equals", "Plus");
            shiftKeyCodeDictionary.Add("F", "CapF");
            shiftKeyCodeDictionary.Add("F1", "F1");
            shiftKeyCodeDictionary.Add("F10", "F10");
            shiftKeyCodeDictionary.Add("F11", "F11");
            shiftKeyCodeDictionary.Add("F12", "F12");
            shiftKeyCodeDictionary.Add("F2", "F2");
            shiftKeyCodeDictionary.Add("F3", "F3");
            shiftKeyCodeDictionary.Add("F4", "F4");
            shiftKeyCodeDictionary.Add("F5", "F5");
            shiftKeyCodeDictionary.Add("F6", "F6");
            shiftKeyCodeDictionary.Add("F7", "F7");
            shiftKeyCodeDictionary.Add("F8", "F8");
            shiftKeyCodeDictionary.Add("F9", "F9");
            shiftKeyCodeDictionary.Add("Five", "Percent");
            shiftKeyCodeDictionary.Add("ForwardSlash", "Question");
            shiftKeyCodeDictionary.Add("Four", "Dollar");
            shiftKeyCodeDictionary.Add("G", "CapG");
            shiftKeyCodeDictionary.Add("Grave", "Tilde");
            shiftKeyCodeDictionary.Add("H", "CapH");
            shiftKeyCodeDictionary.Add("Hyphen", "Underscore");
            shiftKeyCodeDictionary.Add("I", "CapI");
            shiftKeyCodeDictionary.Add("J", "CapJ");
            shiftKeyCodeDictionary.Add("K", "CapK");
            shiftKeyCodeDictionary.Add("L", "CapL");
            shiftKeyCodeDictionary.Add("Left", "Left");
            shiftKeyCodeDictionary.Add("LeftBracket", "LeftBrace");
            shiftKeyCodeDictionary.Add("M", "CapM");
            shiftKeyCodeDictionary.Add("N", "CapN");
            shiftKeyCodeDictionary.Add("Nine", "LeftParenthesis");
            shiftKeyCodeDictionary.Add("O", "CapO");
            shiftKeyCodeDictionary.Add("One", "Exclamation");
            shiftKeyCodeDictionary.Add("P", "CapP");
            shiftKeyCodeDictionary.Add("Period", "GreaterThan");
            shiftKeyCodeDictionary.Add("Q", "CapQ");
            shiftKeyCodeDictionary.Add("R", "CapR");
            shiftKeyCodeDictionary.Add("Right", "Right");
            shiftKeyCodeDictionary.Add("RightBracket", "RightBrace");
            shiftKeyCodeDictionary.Add("S", "CapS");
            shiftKeyCodeDictionary.Add("Semicolon", "Colon");
            shiftKeyCodeDictionary.Add("Seven", "Ampersand");
            shiftKeyCodeDictionary.Add("Six", "Caret");
            shiftKeyCodeDictionary.Add("T", "CapT");
            shiftKeyCodeDictionary.Add("Three", "Number");
            shiftKeyCodeDictionary.Add("Two", "At");
            shiftKeyCodeDictionary.Add("U", "CapU");
            shiftKeyCodeDictionary.Add("Up", "Up");
            shiftKeyCodeDictionary.Add("V", "CapV");
            shiftKeyCodeDictionary.Add("W", "CapW");
            shiftKeyCodeDictionary.Add("X", "CapX");
            shiftKeyCodeDictionary.Add("Y", "CapY");
            shiftKeyCodeDictionary.Add("Z", "CapZ");
            shiftKeyCodeDictionary.Add("Zero", "RightParenthesis");
        }

        private static void InitializeCharactersDictionary()
        {
            charactersDictionary = new Dictionary<string, string>();
            charactersDictionary.Add(" ", "Space");
            charactersDictionary.Add("!", "Exclamation");
            charactersDictionary.Add("'", "Apostrophe");
            charactersDictionary.Add("-", "Hyphen");
            charactersDictionary.Add("#", "Number");
            charactersDictionary.Add("$", "Dollar");
            charactersDictionary.Add("%", "Percent");
            charactersDictionary.Add("&", "Ampersand");
            charactersDictionary.Add("(", "LeftParenthesis");
            charactersDictionary.Add(")", "RightParenthesis");
            charactersDictionary.Add("*", "Asterisk");
            charactersDictionary.Add(",", "Comma");
            charactersDictionary.Add(".", "Period");
            charactersDictionary.Add("/", "ForwardSlash");
            charactersDictionary.Add(":", "Colon");
            charactersDictionary.Add(";", "Semicolon");
            charactersDictionary.Add("?", "Question");
            charactersDictionary.Add("@", "At");
            charactersDictionary.Add("[", "LeftBracket");
            charactersDictionary.Add("\"", "DoubleQuotes");
            charactersDictionary.Add("\\", "Backslash");
            charactersDictionary.Add("]", "RightBracket");
            charactersDictionary.Add("^", "Caret");
            charactersDictionary.Add("_", "Underscore");
            charactersDictionary.Add("`", "Grave");
            charactersDictionary.Add("{", "LeftBrace");
            charactersDictionary.Add("|", "Bar");
            charactersDictionary.Add("}", "RightBrace");
            charactersDictionary.Add("~", "Tilde");
            charactersDictionary.Add("+", "Plus");
            charactersDictionary.Add("<", "LessThan");
            charactersDictionary.Add("=", "Equals");
            charactersDictionary.Add(">", "GreaterThan");
            charactersDictionary.Add("0", "Zero");
            charactersDictionary.Add("1", "One");
            charactersDictionary.Add("2", "Two");
            charactersDictionary.Add("3", "Three");
            charactersDictionary.Add("4", "Four");
            charactersDictionary.Add("5", "Five");
            charactersDictionary.Add("6", "Six");
            charactersDictionary.Add("7", "Seven");
            charactersDictionary.Add("8", "Eight");
            charactersDictionary.Add("9", "Nine");
            charactersDictionary.Add("a", "A");
            charactersDictionary.Add("A", "CapA");
            charactersDictionary.Add("b", "B");
            charactersDictionary.Add("B", "CapB");
            charactersDictionary.Add("c", "C");
            charactersDictionary.Add("C", "CapC");
            charactersDictionary.Add("D", "CapD");
            charactersDictionary.Add("d", "D");
            charactersDictionary.Add("E", "CapE");
            charactersDictionary.Add("e", "E");
            charactersDictionary.Add("F", "CapF");
            charactersDictionary.Add("f", "F");
            charactersDictionary.Add("G", "CapG");
            charactersDictionary.Add("g", "G");
            charactersDictionary.Add("H", "CapH");
            charactersDictionary.Add("h", "H");
            charactersDictionary.Add("I", "CapI");
            charactersDictionary.Add("i", "I");
            charactersDictionary.Add("J", "CapJ");
            charactersDictionary.Add("j", "J");
            charactersDictionary.Add("K", "CapK");
            charactersDictionary.Add("k", "K");
            charactersDictionary.Add("L", "CapL");
            charactersDictionary.Add("l", "L");
            charactersDictionary.Add("M", "CapM");
            charactersDictionary.Add("m", "M");
            charactersDictionary.Add("N", "CapN");
            charactersDictionary.Add("n", "N");
            charactersDictionary.Add("O", "CapO");
            charactersDictionary.Add("o", "O");
            charactersDictionary.Add("P", "CapP");
            charactersDictionary.Add("p", "P");
            charactersDictionary.Add("Q", "CapQ");
            charactersDictionary.Add("q", "Q");
            charactersDictionary.Add("R", "CapR");
            charactersDictionary.Add("r", "R");
            charactersDictionary.Add("S", "CapS");
            charactersDictionary.Add("s", "S");
            charactersDictionary.Add("T", "CapT");
            charactersDictionary.Add("t", "T");
            charactersDictionary.Add("U", "CapU");
            charactersDictionary.Add("u", "U");
            charactersDictionary.Add("V", "CapV");
            charactersDictionary.Add("v", "V");
            charactersDictionary.Add("W", "CapW");
            charactersDictionary.Add("w", "W");
            charactersDictionary.Add("X", "CapX");
            charactersDictionary.Add("x", "X");
            charactersDictionary.Add("Y", "CapY");
            charactersDictionary.Add("y", "Y");
            charactersDictionary.Add("Z", "CapZ");
            charactersDictionary.Add("z", "Z");
           
            charactersDictionary.Add('\u24B6'.ToString(), "Alt");
            charactersDictionary.Add('\u24B7'.ToString(), "Backspace");
            charactersDictionary.Add('\u21EA'.ToString(), "CapsLock");
            charactersDictionary.Add('\u24B8'.ToString(), "Control");
            charactersDictionary.Add('\u24B9'.ToString(), "Delete");
            charactersDictionary.Add('\u2193'.ToString(), "Down");
            charactersDictionary.Add('\u24BA'.ToString(), "End");
            charactersDictionary.Add('\u21B5'.ToString(), "Enter");
            charactersDictionary.Add('\u001B'.ToString(), "Escape");
            charactersDictionary.Add('\u21B8'.ToString(), "Home");
            charactersDictionary.Add('\u24BE'.ToString(), "Insert");
            charactersDictionary.Add('\u2190'.ToString(), "Left");
            charactersDictionary.Add('\u24C2'.ToString(), "Menu");
            charactersDictionary.Add('\u24C3'.ToString(), "NumLock");
            charactersDictionary.Add('\u24CC'.ToString(), "Windows");
            charactersDictionary.Add('\u21DE'.ToString(), "PageDown");
            charactersDictionary.Add('\u21DF'.ToString(), "PageUp");
            charactersDictionary.Add('\u24C7'.ToString(), "PrintScreen");
            charactersDictionary.Add('\u2192'.ToString(), "Right");
            charactersDictionary.Add('\u21E7'.ToString(), "Shift");
            charactersDictionary.Add('\u24C9'.ToString(), "Tab");
            charactersDictionary.Add('\u2191'.ToString(), "Up");
        }

        private static void InitializeNonPrintableCharactersDictionary()
        {

            nonPrintableCharacters = new Dictionary<String, String>();
            nonPrintableCharacters.Add("`ALTL`", '\u24B6'.ToString());
            nonPrintableCharacters.Add("`ALTR`", '\u24B6'.ToString());
            nonPrintableCharacters.Add("`BACKSPACE`", '\u24B7'.ToString());
            nonPrintableCharacters.Add("`CAPS`", '\u21EA'.ToString());
            nonPrintableCharacters.Add("`CTRLL`", '\u24B8'.ToString());
            nonPrintableCharacters.Add("`CTRLR`", '\u24B8'.ToString());
            nonPrintableCharacters.Add("`DELETE`", '\u24B9'.ToString());
            nonPrintableCharacters.Add("`DOWN`", '\u2193'.ToString());
            nonPrintableCharacters.Add("`END`", '\u24BA'.ToString());
            nonPrintableCharacters.Add("`ENTER`", '\u21B5'.ToString());
            nonPrintableCharacters.Add("`ESCAPE`", '\u001B'.ToString());
            nonPrintableCharacters.Add("`HOME`", '\u21B8'.ToString());
            nonPrintableCharacters.Add("`INSERT`", '\u24BE'.ToString());
            nonPrintableCharacters.Add("`LEFT`", '\u2190'.ToString());
            nonPrintableCharacters.Add("`MENU`", '\u24C2'.ToString());
            nonPrintableCharacters.Add("`NUMLOCK`", '\u24C3'.ToString());
            nonPrintableCharacters.Add("`OS`", '\u24CC'.ToString());
            nonPrintableCharacters.Add("`PGDOWN`" , '\u21DE'.ToString());
            //the pgup character points down and  the pgdown character point up here so it makes intutive sense to the user, visually.
            nonPrintableCharacters.Add("`PGUP`" , '\u21DF'.ToString());
            nonPrintableCharacters.Add("`PRNTSCR`", '\u24C7'.ToString());
            nonPrintableCharacters.Add("`RIGHT`", '\u2192'.ToString());
            nonPrintableCharacters.Add("`SHIFTL`", '\u21E7'.ToString());
            nonPrintableCharacters.Add("`SHIFTR`", '\u21E7'.ToString());
            nonPrintableCharacters.Add("`TAB`", '\u24C9'.ToString());
            nonPrintableCharacters.Add("`UP`", '\u2191'.ToString());
            nonPrintableCharacters.Add("`SPACE`", " ");
        }
    }
}
